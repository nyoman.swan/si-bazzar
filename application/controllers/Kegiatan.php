<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kegiatan extends CI_Controller {

	function __construct()
	{
		parent:: __construct();
		$this->load->model('kegiatan_model', 'kegiatan', true);
		$this->load->model('kupon_model', 'kupon', true);

		if(!$this->session->userdata($this->appsession->get()))
		{
			redirect('login', 'refresh');
		}
	}

	public function index()
	{
		$data['kegiatan'] = $this->kegiatan->getkegiatan();
		$data['body'] = $this->load->view('kegiatan', $data, true);
		$this->load->view('template', $data);
	}

	public function detail($id)
	{
		$data['id_kegiatan'] = $id;
		$data['tipe_kupon'] = $this->kupon->gettipe($id);
		$data['kupon'] = $this->kupon->getkupon($id);
		$data['kegiatan'] = $this->kegiatan->getkegiatandetail($id);
		$data['body'] = $this->load->view('kegiatan-detail', $data, true);
		$this->load->view('template', $data);
	}

	public function tambah()
	{
		$this->form_validation->set_rules('nama', 'Nama Kegiatan', 'trim|required');

		if($this->form_validation->run() == false) {
			$data['body'] = $this->load->view('kegiatan-tambah', '', true);
			$this->load->view('template', $data);
		} 
		else {
			$this->kegiatan->tambah();
			redirect('kegiatan','refresh');
		}
	}

	public function edit($id)
	{
		$this->form_validation->set_rules('nama', 'Nama Kegiatan', 'trim|required');

		if($this->form_validation->run() == false) {
			$data['kegiatan'] = $this->kegiatan->getkegiatandetail($id);
			$data['body'] = $this->load->view('kegiatan-edit', $data, true);
			$this->load->view('template', $data);
		} 
		else {
			$this->kegiatan->edit($id);
			redirect('kegiatan','refresh');
		}
	}

	public function hapus($id)
	{
		$this->kegiatan->hapus($id);
		redirect('kegiatan','refresh');
	}
}