<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

  function __construct()
  {
    parent::__construct();
    $this->load->model('user_model', 'user', true);
  }

  public function index()
  {
    if($this->session->userdata($this->appsession->get()))
    {
      redirect('', 'refresh');
    }

    $this->form_validation->set_rules('username', 'Username', 'trim|required');
    $this->form_validation->set_rules('pass', 'Password', 'trim|required|callback_check_database');

    if($this -> form_validation -> run() === false)
    {
      $this->load->view('login', '');
    }
    else
    {
      redirect('redirect', 'refresh');
    }
  }

  function check_database($pass)
  {
    //Field validation succeeded.  Validate against database
    $user = $this->input->post('username');

    //query the database
    $result = $this->user->login($user);

    if($result)
    {
      $user = $result[0];
      if(password_verify($pass, $user->password))
      {
        $sess_array = array();
        $sess_array = array(
          'user_id' => $user->id,
          'username' => $user->username,
          'nama' => $user->nama,
          'role' => $user->role,
          'is_admin' => $user->is_admin
          );
        $this->session->set_userdata($this->appsession->get(), $sess_array);
        $this->session->set_flashdata('alert', 'login');
        return TRUE;
      }
      else
      {
        $this->form_validation->set_message('check_database', 'Password Salah');
        return false;
      }
    }
    else
    {
      $this->form_validation->set_message('check_database', 'Username Salah');
      return false;
    }
  }

  public function logout()
  {
    $this->session->unset_userdata($this->appsession->get());
    // session_destroy();
    $this->session->set_flashdata('alert', 'logout');
    redirect('login', 'refresh');
  }
}