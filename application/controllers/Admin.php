<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	function __construct()
	{
		parent:: __construct();

		if(!$this->session->userdata($this->appsession->get()))
		{
			redirect('login', 'refresh');
		}
		
		$this->load->model('kegiatan_model', 'kegiatan', true);
		$this->load->model('kupon_model', 'kupon', true);
		$this->load->model('user_model', 'user', true);
	}

	public function index()
	{
		$id_user = $this->session->userdata($this->appsession->get())['user_id'];
		$is_admin = $this->session->userdata($this->appsession->get())['is_admin'];
		if($is_admin) {
			$data['kegiatan'] = $this->kegiatan->get();
			$data['jenis_kupon'] = $this->kupon->getjenisall();
			$data['admin_kegiatan'] = $this->kegiatan->getadmin();
		}
		else {
			$data['kegiatan'] = $this->kegiatan->getbyuser($id_user);
			$data['jenis_kupon'] = $this->kupon->getjenisbyuser($id_user);
			$data['admin_kegiatan'] = $this->kegiatan->getadmin();
		}
		$data['body'] = $this->load->view('admin/dashboard', $data, true);
		$this->load->view('admin/template', $data);
	}

	public function adminkegiatan()
	{
		$data['kegiatan'] = $this->kegiatan->get();
		$data['admin_kegiatan'] = $this->kegiatan->getadmin();
		$data['body'] = $this->load->view('admin/adminkegiatan', $data, true);
		$this->load->view('admin/template', $data);
	}

	public function kegiatan($id)
	{
		$data['detail'] = $this->kegiatan->detail($id);
		$data['jenis_kupon'] = $this->kupon->detail($id);
		$data['by_pj'] = $this->kupon->bypj($id);
		$data['pj'] = $this->kupon->getpj($id);
		$data['body'] = $this->load->view('admin/kegiatan', $data, true);
		$this->load->view('admin/template', $data);
	}

	public function ajaxkupon($tahun)
	{
		$data['kupon'] = $this->kupon->ajaxkupon($tahun);
    }

	public function kegiatantambah()
	{
		$this->form_validation->set_rules('nama', 'Nama Kegiatan', 'trim|required');

		if($this->form_validation->run() == false) {
			$data['body'] = $this->load->view('admin/kegiatan-tambah', '', true);
			$this->load->view('admin/template', $data);
		} 
		else {
			$this->kegiatan->tambah();
			redirect('admin','refresh');
		}
	}

	public function kegiatanedit($id)
	{
		$this->form_validation->set_rules('nama', 'Nama Kegiatan', 'trim|required');

		if($this->form_validation->run() == false) {
			$data['detail'] = $this->kegiatan->detail($id);
			$data['jenis_kupon'] = $this->kupon->getjenis($id);
			$data['body'] = $this->load->view('admin/kegiatan-edit', $data, true);
			$this->load->view('admin/template', $data);
		} 
		else {
			$this->kegiatan->edit($id);
			redirect('admin/kegiatan/'.$id,'refresh');
		}
	}
	
	public function pjtambah($id)
	{
		$this->form_validation->set_rules('nama', 'Nama PJ', 'trim|required');

		if($this->form_validation->run() == false) {
			$data['id_kegiatan'] = $id;
			$data['body'] = $this->load->view('admin/pj-tambah', $data, true);
			$this->load->view('admin/template', $data);
		} 
		else {
			$this->kegiatan->tambahpj($id);
			redirect('admin/kegiatan/'.$id,'refresh');
		}
	}
	
	public function pjedit($id_kegiatan, $id)
	{
		$this->form_validation->set_rules('nama', 'Nama PJ', 'trim|required');

		if($this->form_validation->run() == false) {
			$data['id_kegiatan'] = $id_kegiatan;
			$data['detail'] = $this->kegiatan->detailpj($id);
			$data['body'] = $this->load->view('admin/pj-edit', $data, true);
			$this->load->view('admin/template', $data);
		} 
		else {
			$this->kegiatan->editpj($id);
			redirect('admin/kegiatan/'.$id_kegiatan,'refresh');
		}
	}

	public function editpjkupon($id)
	{
		$this->kupon->editpjkupon($id);
		redirect('admin/kegiatan/'.$id,'refresh');
	}
	
	public function editstatuskupon($id)
	{
		$this->kupon->editstatuskupon($id);
		redirect('admin/kegiatan/'.$id,'refresh');
	}

	public function hapuskegiatan($id)
	{
        echo json_encode($this->kegiatan->hapus($id));
    }

	public function hapuspj($id)
	{
        echo json_encode($this->kegiatan->hapuspj($id));
    }

	public function kelolauser()
	{
		$data['user'] = $this->user->getadmin();
		$data['body'] = $this->load->view('admin/kelola-user', $data, true);
		$this->load->view('admin/template', $data);
	}

	public function tambahuser()
	{
		$this->form_validation->set_rules('username', 'Username', 'is_unique[users.username]|trim|required');

		if($this->form_validation->run() == false) {
			$data['body'] = $this->load->view('admin/user-tambah', '', true);
			$this->load->view('admin/template', $data);
		} 
		else {
			$this->user->tambah();
			redirect('admin/kelolauser','refresh');
		}
	}

	public function edituser($id)
	{
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required');

		if($this->form_validation->run() == false) {
			$data['detail'] = $this->user->detail($id);
			$data['body'] = $this->load->view('admin/user-edit', $data, true);
			$this->load->view('admin/template', $data);
		} 
		else {
			$this->user->edit($id);
			redirect('admin/kelolauser','refresh');
		}
	}

	public function hapusadminkegiatan($id)
	{
        echo json_encode($this->kegiatan->hapusadminkegiatan($id));
    }


	public function tambahadminkegiatan()
	{
		$this->form_validation->set_rules('id_kegiatan', 'ID Kegiatan', 'trim|required');

		if($this->form_validation->run() == false) {
			$data['kegiatan'] = $this->kegiatan->get();
			$data['user'] = $this->user->getadmin();
			$data['body'] = $this->load->view('admin/admin-kegiatan-tambah', $data, true);
			$this->load->view('admin/template', $data);
		} 
		else {
			$this->kegiatan->tambahadminkegiatan();
			redirect('admin/adminkegiatan','refresh');
		}
	}

	public function hapususer($id)
	{
        echo json_encode($this->user->hapus($id));
    }

	public function restoreSTBK()
	{
		$this->db->set('status', 1);
		$this->db->where(array('id_kegiatan' => 3, 'status' => 0));
		$this->db->update('kupon');
	}
}
