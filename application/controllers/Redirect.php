<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Redirect extends CI_Controller {

	function __construct()
	{
		parent::__construct();

		if(!$this->session->userdata($this->appsession->get()))
		{
			redirect('login', 'refresh');
		}
	}

	public function index()
	{
		$role = $this->session->userdata($this->appsession->get())['role'];
		if($role == 0)
		{
			redirect('superadmin', 'refresh');
		}
		else if($role == 1)
		{
			redirect('admin', 'refresh');
		}
	}
}