<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kupon extends CI_Controller {

	function __construct()
	{
		parent:: __construct();
		$this->load->model('kupon_model', 'kupon', true);
		$this->load->model('kegiatan_model', 'kegiatan', true);

		if(!$this->session->userdata($this->appsession->get()))
		{
			redirect('login', 'refresh');
		}
	}

	public function tambah($id)
	{
		$this->form_validation->set_rules('keterangan', 'Keterangan', 'trim|required');
		$this->form_validation->set_rules('harga', 'Harga', 'trim|required');
		$this->form_validation->set_rules('jumlah', 'Jumlah', 'trim|required');
		$this->form_validation->set_rules('digit', 'Digit', 'trim|required');

		if($this->form_validation->run() == false) {
			$data['id_kegiatan'] = $id;
			$data['kegiatan'] = $this->kegiatan->getkegiatandetail($id);
			$data['body'] = $this->load->view('tipe-kupon-tambah', $data, true);
			$this->load->view('template', $data);
		} 
		else {
			$this->kupon->tambah($id);
			redirect('kegiatan/detail/'.$id,'refresh');
		}
	}

	public function tambahpj($id)
	{
		$this->kupon->tambahpj($id);
		redirect('kupon/multipleedit/'.$id,'refresh');
	}

	public function edit($id)
	{
		$this->form_validation->set_rules('keterangan', 'Keterangan', 'trim|required');
		$this->form_validation->set_rules('harga', 'Harga', 'trim|required');
		$this->form_validation->set_rules('jumlah', 'Jumlah', 'trim|required');
		$this->form_validation->set_rules('digit', 'Digit', 'trim|required');

		if($this->form_validation->run() == false) {
			$data['kegiatan'] = $this->kegiatan->getkegiatandetail($id);
			$data['tipe_kupon'] = $this->kupon->getdetailtipe($id);
			$data['body'] = $this->load->view('tipe-kupon-edit', $data, true);
			$this->load->view('template', $data);
		} 
		else {
			$this->kupon->edit($id);
			redirect('kegiatan/detail/'.$id,'refresh');
		}
	}

	public function multipleedit($id)
	{
		$this->form_validation->set_rules('pj', 'Penanggung Jawab', 'trim|required');
		$this->form_validation->set_rules('status', 'Status Kupon', 'trim|required');

		if($this->form_validation->run() == false) {
			$data['id_kegiatan'] = $id;
			$data['kegiatan'] = $this->kegiatan->getkegiatandetail($id);
			$data['kupon'] = $this->kupon->getkupon($id);
			$data['pj'] = $this->kupon->getpj($id);
			$data['body'] = $this->load->view('kupon-multiple-edit', $data, true);
			$this->load->view('template', $data);
		} 
		else {
			$this->kupon->multipleedit($id);
			redirect('kegiatan/detail/'.$id,'refresh');
		}
	}

	public function hapus($id)
	{
		$this->kupon->hapus($id);
		redirect('kegiatan/detail/'.$id,'refresh');
	}

	public function kembali($params)
	{
		$param = explode("_", $params);
		$id_kupon = $param[0];
		$id_kegiatan = $param[1];
		$this->kupon->kembali($id_kupon);
		redirect('kegiatan/detail/'.$id_kegiatan,'refresh');
	}

	public function lunas($params)
	{
		$param = explode("_", $params);
		$id_kupon = $param[0];
		$id_kegiatan = $param[1];
		$this->kupon->lunas($id_kupon);
		redirect('kegiatan/detail/'.$id_kegiatan,'refresh');
	}
}