<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kiapauth {

	protected $CI;

    // We'll use a constructor, as you can't directly call a function
    // from a property definition.
    public function __construct()
    {
        // Assign the CodeIgniter super-object
        $this->CI =& get_instance();
    }
     
    public function checksession()
    {
    	if($this->CI->session->userdata('session'))
    	{
    		return true;	
    	}
    	else
    	{
    		return false;
    	}
    }

    public function checkrole($role, $redirect)
    {
    	if($this->checksession())
    	{
    		foreach ($role as $r) {
	    		if($r == $this->CI->session->userdata('session')['role'])
	    		{
	    			return true;
	    		}
    		}
            
            $this->CI->session->set_flashdata('alert', 'acc_denied');
    		redirect($redirect, 'refresh');
    	}
    	else
    	{
    		redirect('login', 'refresh');
    	}
    }
}