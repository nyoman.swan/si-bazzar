<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model {

	function __construct()
	{
		parent::__construct();
	}

	public function getadmin()
	{
		$query = $this->db->query("select u.*
			from users u
			where u.status <> 0
			and u.role = 1
			and u.is_admin = 0");

		if($query->num_rows() > 0)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}

	public function detail($id)
	{
		$query = $this->db->query("select u.*
			from users u
			where u.id='$id'");

		if($query->num_rows() > 0)
		{
			return $query->result()[0];
		}
		else
		{
			return false;
		}
	}

	public function login($username)
  	{
		$query = $this->db->query("select u.*
			from users u
			where u.username='$username' 
				and u.status <> 0");

		if($query -> num_rows() == 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
  	}

	public function getsubadmin()
	{
		$query = $this->db->query("select u.*, s.id_kegiatan
			from sub_admin s
				inner join users u on s.id_user = u.id
			where u.status <> 0");

		if($query->num_rows() > 0)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}

	public function tambah() 
	{
		$username = $this->input->post('username');
		$nama = $this->input->post('nama');
		$alamat = $this->input->post('alamat');
		$password = password_hash($this->input->post('password'), PASSWORD_BCRYPT);

		$data = array(
			'username' => $username,
			'nama' => $nama,
			'alamat' => $alamat,
			'role' => 1,
			'is_admin' => 0,
			'password' => $password
			);

		$this->db->insert('users', $data);
		$this->session->set_flashdata('alert', 'input');
	}

	public function edit($id) 
	{
		$username = $this->input->post('username');
		$nama = $this->input->post('nama');
		$alamat = $this->input->post('alamat');

		$data = array(
			'username' => $username,
			'nama' => $nama,
			'alamat' => $alamat,
			'role' => 1,
			'is_admin' => 0,
			);
		
		if($this->input->post('password') != "") {
			$data['password'] = password_hash($this->input->post('password'), PASSWORD_BCRYPT);
		}

		$this->db->where('id', $id);
		$this->db->update('users', $data);
		$this->session->set_flashdata('alert', 'edit');
	}

	public function hapus($id) 
	{
		$this->db->where('id', $id);
		$this->db->delete('users');
		$this->session->set_flashdata('alert', 'delete');
		return "success";
	}
}