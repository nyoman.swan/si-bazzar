<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kegiatan_model extends CI_Model {

	function __construct()
	{
		parent::__construct();
	}

	public function get() 
	{
		$query = $this->db->query("select k.*, date_format(k.created_at, '%d/%l/%Y %H:%i') as created_at_f
			from kegiatan k
			where k.status <> 0");

		if($query->num_rows() > 0) {
			return $query->result();
		}
		else {
			return false;
		}
	}

	public function getbyuser($id_user) 
	{
		$query = $this->db->query("select k.*, date_format(k.created_at, '%d/%l/%Y %H:%i') as created_at_f
			from kegiatan k
			inner join admin_kegiatan ak on k.id = ak.id_kegiatan
			where k.status <> 0
				and ak.id_user='$id_user'");

		if($query->num_rows() > 0) {
			return $query->result();
		}
		else {
			return false;
		}
	}

	public function getadmin() 
	{
		$query = $this->db->query("select ak.*, u.username, u.nama
			from admin_kegiatan ak
			inner join users u on ak.id_user = u.id
			where u.status <> 0");

		if($query->num_rows() > 0) {
			return $query->result();
		}
		else {
			return false;
		}
	}

	public function detail($id) 
	{
		$query = $this->db->query("select k.*
			from kegiatan k
			where k.id = '$id'");

		if($query->num_rows() > 0) {
			return $query->result()[0];
		}
		else {
			return false;
		}
	}

	public function sequencereset($table)
	{
  		// Exec timer
		ini_set('memory_limit', '-1');

		$query = $this->db->query("select * from $table order by id desc");
		if($query->num_rows() > 0)
		{
			$last_sequence = $query->result()[0]->id + 1;
		}
		else
		{
			$last_sequence = 1;
		}
		$query = $this->db->query("alter table $table auto_increment = $last_sequence");
	}

	public function tambah() 
	{
		$id_user = $this->session->userdata($this->appsession->get())['user_id'];
		$nama_kegiatan = $this->input->post('nama');

		$data = array(
			'nama_kegiatan' => $nama_kegiatan,
			'id_admin' => $id_user
		);

		$this->db->insert('kegiatan', $data);
		$lastid = $this->db->insert_id();

		$jumlah_jenis = $this->input->post('jumlah_jenis');
		// tambah jenis kupon
		$this->tambahjeniskupon($lastid, $jumlah_jenis);

		$this->session->set_flashdata('alert', 'input');
	}

	public function edit($id) 
	{
		$id_user = $this->session->userdata($this->appsession->get())['user_id'];
		$nama_kegiatan = $this->input->post('nama');

		$data = array(
			'nama_kegiatan' => $nama_kegiatan,
			'id_admin' => $id_user
		);

		// update
		$this->db->where('id', $id);
		$this->db->update('kegiatan', $data);
		
		// delete jenis kupon
		$this->db->where('id_kegiatan', $id);
		$this->db->delete('jenis_kupon');
		$this->sequencereset('jenis_kupon');

		// delete kupon
		$this->db->where('id_kegiatan', $id);
		$this->db->delete('kupon');
		$this->sequencereset('kupon');

		$jumlah_jenis = $this->input->post('jumlah_jenis');
		// tambah jenis kupon
		$this->tambahjeniskupon($id, $jumlah_jenis);

		$this->session->set_flashdata('alert', 'edit');
	}

	public function tambahjeniskupon($id_kegiatan, $jumlah) 
	{
		for($i=1;$i<=$jumlah;$i++) {

			if($this->input->post('jenis-kupon-'.$i) != "" &&
			$this->input->post('harga-kupon-'.$i) != "" &&
			$this->input->post('harga-pokok-'.$i) != "" &&
			$this->input->post('digit-kupon-'.$i) != "" &&
			$this->input->post('pertama-kupon-'.$i) != "" &&
			$this->input->post('jumlah-kupon-'.$i) != ""
			) {
				$jenis_kupon = $this->input->post('jenis-kupon-'.$i);
				$harga_kupon = str_replace(",", "", $this->input->post('harga-kupon-'.$i));
				$harga_pokok = str_replace(",", "", $this->input->post('harga-pokok-'.$i));
				$digit_kupon = $this->input->post('digit-kupon-'.$i);
				$no_pertama = intval($this->input->post('pertama-kupon-'.$i));
				$jumlah_kupon = str_replace(",", "", $this->input->post('jumlah-kupon-'.$i));

				$data = array(
					'id_kegiatan' => $id_kegiatan,
					'keterangan' => $jenis_kupon,
					'harga' => $harga_kupon,
					'harga_pokok' => $harga_pokok,
					'digit' => $digit_kupon,
					'no_pertama' => $no_pertama,
					'jumlah' => $jumlah_kupon
				);

				$this->db->insert('jenis_kupon', $data);
				$lastid = $this->db->insert_id();

				// init kupon
				$this->initkupon($id_kegiatan, $lastid, $digit_kupon, $no_pertama, $jumlah_kupon);
			}
		}
	}

	public function initkupon($id_kegiatan, $id_jenis, $digit, $no_pertama, $jumlah)
	{
		for($i=1;$i<=$jumlah;$i++) {
			$no_seri_kupon = str_pad($no_pertama, $digit, "0", STR_PAD_LEFT);

			$data = array(
				'id_kegiatan' => $id_kegiatan,
				'no_seri_kupon' => $no_seri_kupon,
				'id_jenis' => $id_jenis
			);
			$this->db->insert('kupon', $data);
			$no_pertama++;
		}
	}

	public function tambahpj($id_kegiatan)
	{
		$nama = $this->input->post('nama');
		$alamat = $this->input->post('alamat');
		$no_hp = $this->input->post('no_hp');

		$data = array(
			'id_kegiatan' => $id_kegiatan,
			'nama' => $nama,
			'alamat' => $alamat,
			'no_hp' => $no_hp
		);
		$this->db->insert('pj', $data);
		$this->session->set_flashdata('alert', 'input');
	}

	public function detailpj($id) 
	{
		$query = $this->db->query("select *
			from pj
			where id = '$id'");

		if($query->num_rows() > 0) {
			return $query->result()[0];
		}
		else {
			return false;
		}
	}
	
	public function editpj($id)
	{
		$nama = $this->input->post('nama');
		$alamat = $this->input->post('alamat');
		$no_hp = $this->input->post('no_hp');

		$data = array(
			'nama' => $nama,
			'alamat' => $alamat,
			'no_hp' => $no_hp
		);
		$this->db->where('id', $id);
		$this->db->update('pj', $data);

		$this->session->set_flashdata('alert', 'edit');
	}

	public function hapus($id) 
	{
		// kegiatan
		$this->db->set('status', 0);
		$this->db->where('id', $id);
		$this->db->update('kegiatan');

		// kupon
		$this->db->set('status', 0);
		$this->db->where('id_kegiatan', $id);
		$this->db->update('kupon');

		$this->session->set_flashdata('alert', 'delete');
		return "success";
	}

	public function hapuspj($id) 
	{
		$this->db->where('id', $id);
		$this->db->delete('pj');

		// kupon
		$this->db->set('id_pj', null);
		$this->db->where('id_pj', $id);
		$this->db->update('kupon');

		$this->session->set_flashdata('alert', 'delete');
		return "success";
	}

	public function tambahadminkegiatan() 
	{
		$id_kegiatan = $this->input->post('id_kegiatan');
		$id_user = $this->input->post('id_user');

		$data = array(
			'id_kegiatan' => $id_kegiatan,
			'id_user' => $id_user
		);

		$query = $this->db->query("select *
		from admin_kegiatan
		where id_kegiatan = '$id_kegiatan'
			and id_user = '$id_user'");

		if($query->num_rows() > 0) {
			$this->db->where(array('id_kegiatan' => $id_kegiatan, 'id_user' => $id_user));
			$this->db->update('admin_kegiatan', $data);
		}
		else {
			$this->db->insert('admin_kegiatan', $data);
			$this->session->set_flashdata('alert', 'input');
		}
	}

	public function hapusadminkegiatan($id) 
	{
		$this->db->where('id', $id);
		$this->db->delete('admin_kegiatan');
		$this->session->set_flashdata('alert', 'delete');
		return "success";
	}
}