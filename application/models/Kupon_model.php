<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kupon_model extends CI_Model {

	function __construct()
	{
		parent::__construct();
	}

	public function getjenisall() 
	{
		$query = $this->db->query("select k.*
			from jenis_kupon k
			inner join kegiatan kg on k.id_kegiatan = kg.id");

		if($query->num_rows() > 0) {
			return $query->result();
		}
		else {
			return false;
		}
	}

	public function getjenis($id) 
	{
		$query = $this->db->query("select *
			from jenis_kupon
			where id_kegiatan='$id'");

		if($query->num_rows() > 0) {
			return $query->result();
		}
		else {
			return false;
		}
	}

	public function getjenisbyuser($id_user) 
	{
		$query = $this->db->query("select k.*
			from jenis_kupon k
			inner join kegiatan kg on k.id_kegiatan = kg.id
			inner join admin_kegiatan ak on kg.id = ak.id_kegiatan
			where ak.id_user='$id_user'");

		if($query->num_rows() > 0) {
			return $query->result();
		}
		else {
			return false;
		}
	}

	public function detail($id) 
	{
		$query = $this->db->query("select j.keterangan, j.harga, j.harga_pokok, j.jumlah,
			count(
				case when k.status = 1 then k.id end
			) as jumlah_belum_beredar,
			count(
				case when k.status = 2 then k.id end
			) as j_disebar,
			count(
				case when k.status = 3 then k.id end
			) as j_lunas,
			count(
				case when k.status = 4 then k.id end
			) as j_kembali
		from jenis_kupon j
		inner join kupon k on k.id_jenis = j.id
		where k.status <> 0
			and j.id_kegiatan = '$id'
		group by j.keterangan, j.harga, j.harga_pokok, j.jumlah");

		if($query->num_rows() > 0) {
			return $query->result();
		}
		else {
			return false;
		}
	}

	public function bypj($id) 
	{
		$query = $this->db->query("select p.*,
				count(k.id) as jumlah,
				count(
					case when k.status = 3 then k.id end
				) as j_lunas,
				count(
					case when k.status = 4 then k.id end
				) as j_kembali
			from pj p
			inner join kupon k on p.id = k.id_pj
			where k.status <> 0
				and k.id_kegiatan='$id'
			group by p.id");

		if($query->num_rows() > 0) {
			return $query->result();
		}
		else {
			return false;
		}
	}
	
	public function getpj($id) 
	{
		$query = $this->db->query("select pj.id, coalesce(pj.nama, '-') as nama, coalesce(pj.alamat, '-') as alamat, coalesce(pj.no_hp) as no_hp
			from pj
			where pj.id_kegiatan='$id'");

		if($query->num_rows() > 0) {
			return $query->result();
		}
		else {
			return false;
		}
	}

	public function get($id) 
	{
		$query = $this->db->query("select k.id, k.id_kegiatan, k.no_seri_kupon, k.id_jenis, k.id_pj, k.tujuan, k.status,
			j.keterangan, j.harga,
			coalesce(pj.nama, '-') as nama, coalesce(pj.alamat, '-') as alamat, coalesce(pj.no_hp, '-') as no_hp,
			case 
				when k.status = 1 then 'Belum Beredar'
				when k.status = 2 then 'Beredar'
				when k.status = 3 then 'Lunas'
				when k.status = 4 then 'Kembali'
			end as status_f
		from kupon k
			inner join jenis_kupon j on k.id_jenis = j.id
			left join pj on k.id_pj = pj.id
		where k.status <> 0
			and k.id_kegiatan='$id'");

		if($query->num_rows() > 0) {
			return $query->result();
		}
		else {
			return false;
		}
	}
	
	public function editpjkupon($id) 
	{
		$id_user = $this->session->userdata($this->appsession->get())['user_id'];
		$pj = $this->input->post('pj');
		$kupons = json_decode($this->input->post('kupon_dipilih'));

		foreach($kupons as $i) {
			$data = array(
				'id_pj' => $pj,
				'status' => 2,
				'edited_by' => $id_user
			);

			$where = array(
				'id_kegiatan' => $id,
				'no_seri_kupon' => $i
			);

			$this->db->where($where);
			$this->db->update('kupon', $data);
		}

		$this->session->set_flashdata('alert', 'edit');
	}

	public function editstatuskupon($id) 
	{
		$id_user = $this->session->userdata($this->appsession->get())['user_id'];
		$status_kupon = $this->input->post('status_kupon');
		$kupons = json_decode($this->input->post('kupon_dipilih'));

		foreach($kupons as $i) {
			$data = array(
				'status' => $status_kupon,
				'edited_by' => $id_user
			);

			$where = array(
				'id_kegiatan' => $id,
				'no_seri_kupon' => $i
			);
	
			$this->db->where($where);
			$this->db->update('kupon', $data);
		}

		$this->session->set_flashdata('alert', 'edit');
	}

	public function ajaxkupon($id)
	{
		// storing  request (ie, get/post) global array to a variable  
		$requestData['search'] = $this->input->post_get('search', TRUE);
		$requestData['order'] = $this->input->post_get('order', TRUE);
		$requestData['start'] = $this->input->post_get('start', TRUE);
		$requestData['length'] = $this->input->post_get('length', TRUE);
		// $requestData['length'] = 25;
		$requestData['draw'] = $this->input->post_get('draw', TRUE);

		$columns = array( 
		// datatable column index  => database column name
			0 => 'k.no_seri_kupon',
			1 => 'j.keterangan',
			2 => 'j.harga',
			3 => 'pj.nama',
			4 => 'pj.alamat',
			5 => 'pj.no_hp',
			6 => 'k.edited_by',
			7 => 'k.status'
		);

		$query = $this->db->query("select k.id, k.id_kegiatan, k.no_seri_kupon, k.id_jenis, k.id_pj, k.tujuan, k.status,
			j.keterangan, j.harga, format(j.harga, 0) as harga_f,
			coalesce(pj.nama, '-') as nama, coalesce(pj.alamat, '-') as alamat, coalesce(pj.no_hp, '-') as no_hp,
			case 
				when k.status = 1 then 'Belum Beredar'
				when k.status = 2 then 'Beredar <i class=\'ml-1 fa fa-paper-plane text-blue\'></i>'
				when k.status = 3 then 'Lunas <i class=\'ml-1 fa fa-check text-green\'></i>'
				when k.status = 4 then 'Kembali <i class=\'ml-1 fa fa-clock-o text-red\'></i>'
			end as status_f,
			coalesce(u.nama, '-') as edited_by_f
		from kupon k
			inner join jenis_kupon j on k.id_jenis = j.id
			left join pj on k.id_pj = pj.id
			left join users u on k.edited_by = u.id
		where k.status <> 0
			and k.id_kegiatan='$id'");

		$totalData = $query->num_rows();
		$totalFiltered = $totalData;  // when there is no search parameter then total number rows = total number filtered rows.

		$sql = "select k.id, k.id_kegiatan, k.no_seri_kupon, k.id_jenis, k.id_pj, k.tujuan, k.status,
			j.keterangan, j.harga, format(j.harga, 0) as harga_f,
			coalesce(pj.nama, '-') as nama, coalesce(pj.alamat, '-') as alamat, coalesce(pj.no_hp, '-') as no_hp,
			case 
				when k.status = 1 then 'Belum Beredar'
				when k.status = 2 then 'Beredar <i class=\'ml-1 fa fa-paper-plane text-blue\'></i>'
				when k.status = 3 then 'Lunas <i class=\'ml-1 fa fa-check text-green\'></i>'
				when k.status = 4 then 'Kembali <i class=\'ml-1 fa fa-clock-o text-red\'></i>'
			end as status_f,
			coalesce(u.nama, '-') as edited_by_f
		from kupon k
			inner join jenis_kupon j on k.id_jenis = j.id
			left join pj on k.id_pj = pj.id
			left join users u on k.edited_by = u.id
		where k.status <> 0
			and k.id_kegiatan='$id'";

		// Search data table
		if( !empty($requestData['search']['value']) ) {   // if there is a search parameter, $requestData['search']['value'] contains search parameter
			$sql.=" and ( k.no_seri_kupon like '%".$requestData['search']['value']."%'";
			$sql.=" or j.keterangan like '%".$requestData['search']['value']."%'";
			$sql.=" or j.harga like '%".$requestData['search']['value']."%'";
			if(
				$requestData['search']['value'] == 'Belum Beredar' || $requestData['search']['value'] == 'belum beredar'
			) {
				$sql.=" or k.status = 1";
			}
			else if(
				$requestData['search']['value'] == 'Beredar' || $requestData['search']['value'] == 'beredar'
			) {
				$sql.=" or k.status = 2";
			}
			else if(
				$requestData['search']['value'] == 'Lunas' || $requestData['search']['value'] == 'lunas'
			) {
				$sql.=" or k.status = 3";
			}
			else if(
				$requestData['search']['value'] == 'Kembali' || $requestData['search']['value'] == 'kembali'
			) {
				$sql.=" or k.status = 4";
			}
			$sql.=" or pj.nama like '%".$requestData['search']['value']."%' )";
		}

		$query = $this->db->query($sql);
		$totalFiltered = $query->num_rows(); // when there is a search parameter then we have to modify total number filtered rows as per search result. 

		$sql.=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."  LIMIT ".$requestData['length']." OFFSET ".$requestData['start']."   ";
		/* $requestData['order'][0]['column'] contains colmun index, $requestData['order'][0]['dir'] contains order such as asc/desc  */	

		$query = $this->db->query($sql);

		$data = array();
		if($query->num_rows() > 0) {
		    $result = $query->result();
		    $nestedData=array();
		    $row_num = $requestData['start'] + 1;
		    $index = 0;

		    foreach($result as $row )
		    {
		        $nestedData[0] = $row->no_seri_kupon;
				$nestedData[1] = $row->no_seri_kupon;
				$nestedData[2] = $row->keterangan;
		        $nestedData[3] = $row->harga_f;
		        $nestedData[4] = $row->nama;
		        $nestedData[5] = $row->alamat;
		        $nestedData[6] = $row->no_hp;
		        $nestedData[7] = $row->edited_by_f;
		        $nestedData[8] = $row->status_f;
			
				$data[$index] = $nestedData;
		    	$row_num++;
		    	$index++;
		    }
		}

		$json_data = array(
			"draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
			"recordsTotal"    => intval( $totalData ),  // total number of records
			"recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data"            => $data,   // total data array
			"query"            => $sql
			);

		echo json_encode($json_data);  // send data as json format;
  	}








	public function tambahpj($id) 
	{
		$nama = $this->input->post('nama');
		$alamat = $this->input->post('alamat');
		$no_hp = $this->input->post('no_hp');

		$data = array(
			'id_kegiatan' => $id,
			'nama' => $nama,
			'alamat' => $alamat,
			'no_hp' => $no_hp
			);

		$this->db->insert('sibaz_pj', $data);
		$this->session->set_flashdata('alert', 'input');
	}

	public function tambah($id) 
	{
		$id_user = $this->session->userdata($this->appsession->get())['user_id'];
		$keterangan = $this->input->post('keterangan');
		$harga = str_replace(",", "", $this->input->post('harga'));
		$jumlah = str_replace(",", "", $this->input->post('jumlah'));
		$digit = intval($this->input->post('digit'));
		$format = '%0' . $digit . 'd';

		$data = array(
			'id_kegiatan' => $id,
			'keterangan' => $keterangan,
			'harga' => $harga,
			'created_by' => $id_user
			);

		$this->db->insert('sibaz_tipe_kupon', $data);
		$lastid = $this->db->insert_id();

		for($i=1;$i<=$jumlah;$i++)
		{
			$data_kupon = array(
				'no_seri_kupon' => sprintf($format, $i),
				'id_tipe' => $lastid
				);

			$this->db->insert('sibaz_kupon', $data_kupon);
		}

		$this->session->set_flashdata('alert', 'input');
	}

	public function edit($id) 
	{
		$keterangan = $this->input->post('keterangan');
		$harga = str_replace(",", "", $this->input->post('harga'));
		$jumlah = str_replace(",", "", $this->input->post('jumlah'));
		$digit = intval($this->input->post('digit'));
		$format = '%0' . $digit . 'd';

		$data = array(
			'keterangan' => $keterangan,
			'harga' => $harga,		
			'digit' => $digit	
			);

		$this->db->where('id_tipe', $id);
		$this->db->update('sibaz_tipe_kupon', $data);

		$query = $this->db->query("select k.*, count(kp.id_kupon) as jumlah_kupon
			from sibaz_tipe_kupon k
			inner join sibaz_kupon kp on k.id_tipe = kp.id_tipe
			where k.id_tipe='$id'");

		if($query->num_rows() > 0) {
			$result = $query->result()[0];

			// do nothing
			if($jumlah == $result->jumlah_kupon)
			{

			}

			// if input is greater
			else if($jumlah > $result->jumlah_kupon)
			{
				$start = $result->jumlah_kupon + 1;
				for($i=$start;$i<=$jumlah;$i++)
				{
					$data_kupon = array(
						'no_seri_kupon' => sprintf($format, $i),
						'id_tipe' => $id
						);

					$this->db->insert('sibaz_kupon', $data_kupon);
				}
			}

			// if input is lower
			else if($jumlah < $result->jumlah_kupon)
			{
				$selisih = $result->jumlah_kupon-$jumlah;
				$query = $this->db->query("select kp.*
					from sibaz_kupon kp
					where kp.id_tipe='$id'
					order by kp.no_seri_kupon
					limit $jumlah, $selisih");

				if($query->num_rows() > 0) {
					$result = $query->result();
					foreach($result as $row)
					{
						$this->db->where('id_kupon', $row->id_kupon);
						$this->db->delete('sibaz_kupon');
					}
				}
			}
		}

		// reformat no seri kupon
		$query = $this->db->query("select kp.*
			from sibaz_kupon kp
			where kp.id_tipe='$id'
			order by kp.no_seri_kupon");

		if($query->num_rows() > 0) {
			$result = $query->result();
			foreach($result as $row)
			{
				$no_urut = intval($row->no_seri_kupon);
				$data_kupon = array(
					'no_seri_kupon' => sprintf($format, $no_urut)
					);

				$this->db->where('id_kupon', $row->id_kupon);
				$this->db->update('sibaz_kupon', $data_kupon);
			}
		}

		$this->session->set_flashdata('alert', 'edit');
	}

	public function multipleedit($id) 
	{
		$id_pj = $this->input->post('pj');
		$status = $this->input->post('status');
		$tujuan = $this->input->post('tujuan');
		$noseri = array_unique($this->input->post('noseri'));

		foreach($noseri as $row)
		{
			$data = array(
			'id_pj' => $id_pj,
			'status' => $status,
			'tujuan' => $tujuan
			);

			$this->db->where('id_kupon', $row);
			$this->db->update('sibaz_kupon', $data);
		}

		$this->session->set_flashdata('alert', 'input');
	}

	public function hapus($id) 
	{
		$this->db->set('status', 0);
		$this->db->where('id_tipe', $id);
		$this->db->update('sibaz_tipe_kupon');

		$this->session->set_flashdata('alert', 'delete');
	}

	public function kembali($id) 
	{
		$this->db->set('status', 3);
		$this->db->where('id_kupon', $id);
		$this->db->update('sibaz_kupon');

		$this->session->set_flashdata('alert', 'edit');
	}

	public function lunas($id) 
	{
		$this->db->set('status', 4);
		$this->db->where('id_kupon', $id);
		$this->db->update('sibaz_kupon');

		$this->session->set_flashdata('alert', 'edit');
	}
}