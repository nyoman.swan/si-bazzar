<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master_model extends CI_Model {

	function __construct()
	{
		parent::__construct();
	}

	public function getsatuan() 
	{
		$query = $this->db->query("select m.*
			from m_satuan m
			order by m.satuan");

		if($query->num_rows() > 0) {
			return $query->result();
		}
		else {
			return false;
		}
	}

	public function getsub() 
	{
		$query = $this->db->query("select m.*
			from m_sub_pengeluaran m
			order by m.sub_pengeluaran");

		if($query->num_rows() > 0) {
			return $query->result();
		}
		else {
			return false;
		}
	}
}