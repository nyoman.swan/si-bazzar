<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="row">
      <div class="col-md-10 col-sm-8 col-xs-12">
        <h3>
          <i class="fa fa-th-large text-orange"></i> <a href="<?=site_url('admin')?>">SI-Bazzar</a>
          &nbsp;<i class="fa fa-angle-right"></i> Kelola User
        </h3>
      </div>
      <div class="col-md-2 col-sm-4 col-xs-6 pull-right">
      </div>
    </div>
  </section>

  <!-- Main content -->
  <section class="content">

    <div class="col-md-12">
        <a href="<?=site_url('admin/tambahuser')?>" class="btn btn-success pull-right mb-1"><i class="fa fa-plus"></i> Tambah User</a>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="box box-warning">
          <div class="box-header">
            <h3 class="box-title">Kelola User</h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body table-responsive">
            <table class="table data-table table-bordered table-hover">
              <thead>
              <tr>
                <th style="width: 2rem;"></th>
                <th>Username</th>
                <th>Nama</th>
                <th>Alamat</th>
                <th></th>
              </tr>
              </thead>
              <tbody>

                <?php
                  $no=1;
                  if(is_array($user) || is_object($user)) :
                  foreach ($user as $row) :
                ?>

                <tr>
                  <td><?=$no?>.</td>
                  <td><?=$row->username?></a></td>
                  <td><?=$row->nama?></a></td>
                  <td><?=$row->alamat?></a></td>
                  <td align="center">
                    <div class="btn-group">
                      <a href="<?=site_url('admin/edituser/'.$row->id)?>" class="btn btn-default"><i class="fa fa-edit"></i></a>
                      <a href="#" data-params="<?=$row->id?>" class="btn btn-default hapus-data"><i class="fa fa-trash text-red"></i></a>
                    </div>
                  </td>
                </tr>

              <?php
                $no++;
                endforeach;
                endif;
              ?>

              </tbody>
            </table>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
    </div>
    <!-- /.row -->

  </section>
  <!-- /.content -->
</div>

<!-- Jquery Validate -->
<script src="<?=base_url('assets/js/jquery-validate.js')?>"></script>

<script>
$('.user').addClass('active');
$( document ).ready(function() {
  $('.select2').select2()
  $(".data-table").DataTable();
  $(document).on('click', '.hapus-data', function(e) {
    e.preventDefault();
    Swal.fire({
      title: 'Konfirmasi',
      text: "Hapus Data?",
      icon: 'warning',
    }).then((result) => {
      if (result.value == true) {
        var id = this.dataset.params;
        $.ajax({   
          type: "POST",
          dataType: "html",
          url: "<?=base_url('admin/hapususer/')?>" + id,   
          success: function(data){
            data = JSON.parse(data);
            if(data == "success") {
              window.location.href = "<?=site_url('admin')?>";
            }
          }
        });
      }
    })
  });
});
</script>