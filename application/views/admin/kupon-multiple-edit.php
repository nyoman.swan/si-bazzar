<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jstree/3.2.1/themes/default/style.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jstree/3.2.1/jstree.min.js"></script>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="row">
      <div class="col-md-10 col-sm-8 col-xs-12">
        <h4>
          <i class="fa fa-home"></i> <a href="<?=base_url()?>">List Kegiatan</a>
          &nbsp;<i class="fa fa-angle-right"></i> <a href="<?=base_url('kegiatan/detail/'.$kegiatan->id_kegiatan)?>"><?=$kegiatan->nama_kegiatan?></a>
          &nbsp;<i class="fa fa-angle-right"></i> Multiple Edit Kupon
        </h4>
      </div>
      <div class="col-md-2 col-sm-4 col-xs-6 pull-right">
      </div>
    </div>
  </section>

  <!-- Main content -->
  <section class="content">

    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="box box-warning">
          <div class="box-header">
            <h3 class="box-title">Multiple Edit Kupon</h3>
          </div>
          <?php
            echo form_open('kupon/multipleedit/'.$id_kegiatan, array('method' => 'POST', 'role' => 'form', 'enctype' => 'multipart/form-data', 'class' => 'form-horizontal'));
            echo validation_errors();
          ?>
            <div class="box-body">
              <div class="form-group">
                <label class="col-sm-2 control-label">Penanggungjawab</label>
                <div class="col-sm-7">
                  <select required="" class="form-control" name="pj">
                    <option disabled="" selected="">Pilih</option>
                    <option value="0">Kosongkan</option>
                    <?php
                      if(is_object($pj) || is_array($pj)) :
                        foreach ($pj as $row) :
                    ?>
                    <option value="<?=$row->id_pj?>"><?=$row->nama . " - " . $row->alamat?></option>
                    <?php
                        endforeach;
                      endif;
                    ?>
                  </select>
                </div>
                <div class="col-md-2">
                  <a data-toggle="modal" data-target="#modal-tambah" data-params="<?=$id_kegiatan?>" class="btn btn-flat btn-info"><i class="fa fa-plus"></i> Tambah Penanggung Jawab</a>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label">Status Kupon</label>
                <div class="col-sm-7">
                  <select required="" class="form-control" name="status">
                    <option disabled="" selected="">Pilih</option>
                    <option value="0">Kosongkan</option>
                    <option value="1">Belum Disebar</option>
                    <option value="2" selected="">Proses</option>
                    <option value="3">Kembali</option>
                    <option value="4">Lunas</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label">Tujuan</label>
                <div class="col-sm-7">
                  <input type="text" name="tujuan" class="form-control" placeholder="Tujuan">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label">No Seri Kupon</label>
                <div class="col-sm-3">
                    <p>Filter: <input type="text" id="search" /><button id="clear">Clear</button></p>
                  <div id="jstree" style="height: 350px; overflow: auto;">
                    <ul>
                      <?php
                        if(is_object($kupon) || is_array($kupon)) :
                          foreach ($kupon as $row) :
                      ?>
                      <li data-params="<?=$row->id_kupon?>"> No Seri Kupon - <?php echo "[" . $row->no_seri_kupon . "] "; ?></li>
                      <?php
                          endforeach;
                        endif;
                      ?>
                    </ul>
                  </div>
                </div>
                <div class="col-sm-3">
                  <div style="height: 350px; overflow: auto;">
                    <p>No Seri Kupon Dipilih:</p>
                    <ul id="output"></ul>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
              <div class="pull-right">
                <a href="<?=site_url('kegiatan/detail/'.$id_kegiatan)?>" class="btn btn-default mr-10"><i class="fa fa-times"></i> Batal</a>
                <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Simpan</button>
              </div>
            </div>
            <!-- /.box-footer -->
          </form>
        </div>
        <!-- /.box -->
      </div>
    </div>
    <!-- /.row -->

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<div class="modal fade" id="modal-tambah" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <?php
        echo form_open('kupon/tambahpj/'.$id_kegiatan, array('method' => 'POST', 'role' => 'form', 'enctype' => 'multipart/form-data'));
        echo validation_errors();
      ?>
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><i class="fa fa-edit"></i> Tambah Penanggung Jawab</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <div class="alert alert-success alert-dismissible">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
              <p><i class="icon fa fa-warning"></i> Lengkapi form berikut.</p>
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group">
                <label class="dp-block">Nama Penanggung Jawab</label>
                <input class="form-control" type="text" name="nama" placeholder="Nama" value="">
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group">
                <label class="dp-block">Alamat</label>
                <input class="form-control" type="text" name="alamat" placeholder="Alamat" value="">
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group">
                <label class="dp-block">No HP</label>
                <input class="form-control" type="text" name="no_hp" placeholder="No HP" value="">
            </div>
          </div>
        </div>
      </div>
      <div style="margin-top: 24px;" class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal"><i style="margin-right: 5px;" class="fa fa-times"></i> Close</button>
        <button type="submit" class="btn btn-default pull-right"><i style="margin-right: 5px;" class="fa fa-check text-green"></i> Simpan</button>
      </div>
      </form>
    </div>
      <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

<script type="text/javascript">
$('.dashboard').addClass('active');
  // Jquery Dependency

  $("input[data-type='currency']").on({
      keyup: function() {
        formatCurrency($(this));
      },
      blur: function() { 
        formatCurrency($(this), "blur");
      }
  });


  function formatNumber(n) {
    // format number 1000000 to 1,234,567
    return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",")
  }

  function formatCurrency(input, blur) {
    // appends $ to value, validates decimal side
    // and puts cursor back in right position.
    
    // get input value
    var input_val = input.val();
    
    // don't validate empty input
    if (input_val === "") { return; }
    
    // original length
    var original_len = input_val.length;

    // initial caret position 
    var caret_pos = input.prop("selectionStart");
      
    // check for decimal
    if (input_val.indexOf(".") >= 0) {

      // get position of first decimal
      // this prevents multiple decimals from
      // being entered
      var decimal_pos = input_val.indexOf(".");

      // split number by decimal point
      var left_side = input_val.substring(0, decimal_pos);
      var right_side = input_val.substring(decimal_pos);

      // add commas to left side of number
      left_side = formatNumber(left_side);

      // validate right side
      right_side = formatNumber(right_side);
      
      // On blur make sure 2 numbers after decimal
      if (blur === "blur") {
        right_side;
      }
      
      // Limit decimal to only 2 digits
      right_side = right_side.substring(0, 2);

      // join number by .
      input_val = left_side + "." + right_side;

    } else {
      // no decimal entered
      // add commas to number
      // remove all non-digits
      input_val = formatNumber(input_val);
      input_val = input_val;
      
      // final formatting
      if (blur === "blur") {
        input_val;
      }
    }
    
    // send updated string to input
    input.val(input_val);

    // put caret back in the right position
    var updated_len = input_val.length;
    caret_pos = updated_len - original_len + caret_pos;
    input[0].setSelectionRange(caret_pos, caret_pos);
  }
</script>
<script>
  $('#jstree').jstree({
    'plugins': ['search', 'checkbox', 'wholerow'],
    'core': {
      'animation': false,
      'themes': {
        'icons': false,
      },
      'search': {
        'show_only_matches': true,
        'show_only_matches_children': true
      }
    }
  })
  
  $('#search').on("keyup change", function () {
    $('#jstree').jstree(true).search($(this).val())
  })
  
  $('#clear').click(function (e) {
      e.preventDefault();
    $('#search').val('').change().focus()
  })
  
  $('#jstree').on('changed.jstree', function (e, data) {
    var objects = data.instance.get_selected(true)
    var leaves = $.grep(objects, function (o) { return data.instance.is_leaf(o) })
    var list = $('#output')
    var i = 0
    list.empty()
    $.each(leaves, function (i, o) {
      $('<li/>').text(o.text).attr('id', o.id).appendTo(list);
      $('#' + o.id).append('<input name="noseri[]" type="hidden" value="' + o.data.params + '"/>');
      console.log($('#' + o.id));
      i++;
    })
  })
</script>