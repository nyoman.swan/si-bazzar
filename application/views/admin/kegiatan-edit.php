<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="row">
      <div class="col-md-10 col-sm-8 col-xs-12">
        <h3>
          <i class="fa fa-th-large text-orange"></i> <a href="<?=site_url('admin')?>">SI-Bazzar</a>
          &nbsp;<i class="fa fa-angle-right"></i> Edit Kegiatan
        </h3>
      </div>
      <div class="col-md-2 col-sm-4 col-xs-6 pull-right">
      </div>
    </div>
  </section>

  <!-- Main content -->
  <section class="content">

    <div class="row">
        <div class="col-md-12">
            <div class="box box-danger box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title"><i class="fa fa-warning mr-1"></i> PERINGATAN! <i class="fa fa-warning ml-1"></i></h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <div class="box-body">
                  Melakukan edit/update data kegiatan akan me-reset data kupon. <br><br>
                  Data kupon yang telah disimpan sebelumnya akan hilang :( <br><br>
                  Lanjutkan jika anda memang benar-benar yakin.
                </div>
            </div>
        </div>
    </div>

    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="box box-warning">
          <div class="box-header">
            <h3 class="box-title">Edit Kegiatan</h3>
          </div>
          <?php
            echo form_open('admin/kegiatan/edit/'.$detail->id, array('method' => 'POST', 'role' => 'form', 'enctype' => 'multipart/form-data', 'class' => 'form-horizontal', 'id' => 'dataform'));
            echo validation_errors();
          ?>
            <div class="box-body">
              <div class="form-group row">
                <label class="col-sm-1 control-label">Nama Kegiatan</label>
                <div class="col-sm-10">
                  <input name="nama" required="" class="form-control" placeholder="Nama Kegiatan" type="text" value="<?=$detail->nama_kegiatan?>">
                </div>
              </div>

              <div class="form-group row">
                <label class="col-sm-1 control-label">Jenis Kupon</label>
                <div class="col-sm-10">
                  <a href="javascript:void(0);" id="tambah-jenis" class="btn btn-default btn-sm"><i class="fa fa-plus text-aqua"></i> Tambah Jenis Kupon</a>
                </div>
              </div>

              <input type="hidden" name="jumlah_jenis" id="jumlah_jenis" value="<?php echo count($jenis_kupon); ?>">

              <div id="container-jk">

                <?php
                  if(is_array($jenis_kupon)):
                    $no=1;
                    foreach($jenis_kupon as $row):
                ?>

                <div class="form-group row cont-jenis-<?=$no?>">
                  <p class="col-sm-1 control-label"></p>
                  <div class="col-sm-3">
                    <label><?=$no?>. Keterangan</label>
                    <input name="jenis-kupon-<?=$no?>" required="" class="form-control" placeholder="Keterangan" type="text" value="<?=$row->keterangan?>">
                  </div>
                  <div class="col-sm-1">
                    <label>Digit</label>
                    <input name="digit-kupon-<?=$no?>" required="" class="form-control" placeholder="Digit" type="number" value="<?=$row->digit?>">
                  </div>
                  <div class="col-sm-1">
                    <label>No. Pertama</label>
                    <input name="pertama-kupon-<?=$no?>" required="" class="form-control" placeholder="No Pertama" type="number" value="<?=$row->no_pertama?>">
                  </div>
                  <div class="col-sm-1">
                    <label>Jml. (Lb.)</label>
                    <input name="jumlah-kupon-<?=$no?>" required="" class="form-control currency" placeholder="Jumlah" type="text" value="<?=number_format($row->jumlah, 0)?>">
                  </div>
                  <div class="col-sm-2">
                    <label>Harga Satuan (Rp.)</label>
                    <input name="harga-kupon-<?=$no?>" required="" class="form-control currency" placeholder="Harga" type="text" value="<?=number_format($row->harga, 0)?>">
                  </div>
                  <div class="col-sm-2">
                    <label>Harga Pokok (Rp.)</label>
                    <input name="harga-pokok-<?=$no?>" required="" class="form-control currency" placeholder="Harga Pokok" type="text" value="<?=number_format($row->harga_pokok, 0)?>">
                  </div>
                  <?php if($no==count($jenis_kupon)): ?>
                  <div class="col-sm-1">
                    <label class="opacity-0"><?=$no?></label>
                    <div>
                      <a href="javascript:void(0);" class="btn btn-default btn-sm hapus-jenis" data-params="<?=$no?>"><i class="fa fa-trash text-red"></i></a>
                    </div>
                  </div>
                  <?php endif; ?>
                </div>
                
                <?php
                      $no++;
                    endforeach;
                  endif;
                ?>

              </div>

            </div>
            <!-- /.box-body -->
            <div class="box-footer">
              <div class="pull-right">
                <a href="<?=site_url('admin/kegiatan/'.$detail->id)?>" class="btn btn-default mr-10"><i class="fa fa-times"></i> Batal</a>
                <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Simpan</button>
              </div>
            </div>
            <!-- /.box-footer -->
          </form>
        </div>
        <!-- /.box -->
      </div>
    </div>
    <!-- /.row -->

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script src="<?=base_url('assets/js/format-currency.js')?>"></script>
<!-- Jquery Validate -->
<script src="<?=base_url('assets/js/jquery-validate.js')?>"></script>
<script>
$('.dashboard').addClass('active');
$( document ).ready(function() {

  $('#dataform').validate({ // initialize the plugin
    rules: {
      'nama': {
        required: true
      },
      'jenis-kupon-1': {
        required: true
      },
      'digit-kupon-1': {
        required: true
      },
      'pertama-kupon-1': {
        required: true
      },
      'jumlah-kupon-1': {
        required: true
      },
      'harga-kupon-1': {
        required: true
      }
    },
    submitHandler: function (form) {
      Swal.fire({
        title: 'Lanjutkan proses?',
        text: "Data kupon akan di-reset",
        icon: 'warning',
      }).then((result) => {
        if (result.value == true) {
          form.submit();
        }
      })
    }
  });

  $(document).on('keyup', '.currency', function(){
    formatCurrency($(this));
  });

  $(document).on('blur', '.currency', function(){
    formatCurrency($(this), "blur");
  });

  var jumlah_jenis = "<?php echo count($jenis_kupon); ?>";
  $('#tambah-jenis').on('click', function(){
    jumlah_jenis++;
    $('#jumlah_jenis').val(jumlah_jenis);
    $('.hapus-jenis').addClass('hidden');
    var html = '<div class="form-group row cont-jenis-' + jumlah_jenis + '">' +
      '<p class="col-sm-1 control-label"></p>' +
      '<div class="col-sm-3">' +
      '<label>' + jumlah_jenis + '. Keterangan</label>' +
      '<input name="jenis-kupon-' + jumlah_jenis + '" required="" class="form-control" placeholder="Keterangan" type="text">' +
      '</div>' +
      '<div class="col-sm-1">' +
      '<label>Digit</label>' +
      '<input name="digit-kupon-' + jumlah_jenis + '" required="" class="form-control" placeholder="Digit" type="number">' +
      '</div>' +
      '<div class="col-sm-1">' +
      '<label>No. Pertama</label>' +
      '<input name="pertama-kupon-' + jumlah_jenis + '" required="" class="form-control" placeholder="Digit" type="number">' +
      '</div>' +
      '<div class="col-sm-1">' +
      '<label>Jml. (Lb.)</label>' +
      '<input name="jumlah-kupon-' + jumlah_jenis + '" required="" class="form-control currency" placeholder="Jumlah" type="text">' +
      '</div>' +
      '<div class="col-sm-2">' +
      '<label>Harga Satuan (Rp.)</label>' +
      '<input name="harga-kupon-' + jumlah_jenis+ '" required="" class="form-control currency" placeholder="Harga Satuan" type="text">' +
      '</div>' +
      '<div class="col-sm-2">' +
      '<label>Harga Pokok (Rp.)</label>' +
      '<input name="harga-pokok-' + jumlah_jenis+ '" required="" class="form-control currency" placeholder="Harga Pokok" type="text">' +
      '</div>' +
      '<div class="col-sm-1">' +
      '<label class="opacity-0">' + jumlah_jenis + '</label>' +
      '<div><a href="javascript:void(0);" class="btn btn-default btn-sm hapus-jenis" data-params="' + jumlah_jenis + '"><i class="fa fa-trash text-red"></i></a></div>' +
      '</div>' +
      '</div>';
    $('#container-jk').append(html);
  });

  $(document).on('click', '.hapus-jenis', function(){
    $('.cont-jenis-' + this.dataset.params).remove();
    jumlah_jenis--;
    $('#jumlah_jenis').val(jumlah_jenis);
    if(jumlah_jenis>1) {
      $('.cont-jenis-' + jumlah_jenis + ' .hapus-jenis').removeClass('hidden');
    }
  });
});
</script>