<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>SI Bazzar</title>
    <link rel="shortcut icon" href="<?=base_url('assets/dist/img/favicon.png')?>">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="<?=base_url('assets/bower_components/bootstrap/dist/css/bootstrap.min.css')?>">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?=base_url('assets/bower_components/font-awesome/css/font-awesome.min.css')?>">
    <!-- Ionicons -->
    <link rel="stylesheet" href="<?=base_url('assets/bower_components/Ionicons/css/ionicons.min.css')?>">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?=base_url('assets/dist/css/AdminLTE.min.css')?>">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
        folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<?=base_url('assets/dist/css/skins/_all-skins.css')?>">
    <!-- DataTables -->
    <link rel="stylesheet" href="<?=base_url('assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')?>">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="<?=base_url('assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')?>">
    <!-- Google Font -->
    <link rel="stylesheet" href="<?=base_url('assets/dist/fonts-googleapis.com.css')?>">
    <!-- Select2 -->
    <link rel="stylesheet" href="<?=base_url('assets/bower_components/select2/dist/css/select2.css')?>">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="<?=base_url('assets/css/custom.css')?>">
    
    <!-- jQuery 3 -->
    <script src="<?=base_url('assets/bower_components/jquery/dist/jquery.min.js')?>"></script>
    <script src="<?=base_url('assets/dist/js/jquery.confirm.js')?>"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="<?=base_url('assets/bower_components/bootstrap/dist/js/bootstrap.min.js')?>"></script>
    <!-- DataTables -->
    <script src="<?=base_url('assets/bower_components/datatables.net/js/jquery.dataTables.min.js')?>"></script>
    <script src="<?=base_url('assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')?>"></script>
    <!-- Select2 -->
    <script src="<?=base_url('assets/bower_components/select2/dist/js/select2.js')?>"></script>
    <!-- Sweet Alert -->
    <script src="<?=base_url('assets/js/sweetalert.js')?>"></script>
  </head>
  <body class="hold-transition skin-yellow sidebar-mini sidebar-collapse">
    <div class="wrapper">
      <header class="main-header">
        <!-- Logo -->
        <a href="<?=site_url('dashboard')?>" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>SB</b></span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>SI</b>Bazzar</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar" id="side_menu">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="<?=base_url('assets/dist/img/desa-kapal.png')?>" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p><?=$this->session->userdata($this->appsession->get())['nama'];?></p>
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu" data-widget="tree">
            <li class="header">NAVIGASI UTAMA</li>
            <li class="dashboard"><a href="<?=site_url('admin')?>"><i class="fa fa-th-large"></i> <span>SI Bazzar</span></a></li>
            <?php if($this->session->userdata($this->appsession->get())['is_admin']): ?>
            <li class="admin"><a href="<?=site_url('admin/adminkegiatan')?>"><i class="fa fa-cubes"></i> <span>Admin Kegiatan</span></a></li>
            <li class="user"><a href="<?=site_url('admin/kelolauser')?>"><i class="fa fa-users"></i> <span>Kelola User</span></a></li>
            <?php endif; ?>
            <li class="header">LOGOUT</li>
            <li><a id="logout" href="<?=site_url('login/logout')?>"><i class="fa fa-power-off text-red"></i> <span>Log Out</span></a></li>
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>

      <!-- content -->
      <?=$body?>

      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Version</b> 1.0.0 c
        </div>
        <strong>&copy; 2017 - <?=date('Y')?> | Purwarupa oleh: <a href="https://www.instagram.com/nyoman_elin/" target="_blank">Nyoman Swantara</a> | <a href="https://www.instagram.com/st.wira_karya_kapal/" target="_blank">Sekaa Teruna Wira Karya Br Cepaka Kapal</a> | <a href="mailto:nyoman.swan@gmail.com"><i class="fa fa-envelope-o"></i> nyoman.swan@gmail.com</a></strong>
      </footer>
    </div>
    <!-- ./wrapper -->

    <!-- Slimscroll -->
    <script src="<?=base_url('assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')?>"></script>
    <!-- FastClick -->
    <script src="<?=base_url('assets/bower_components/fastclick/lib/fastclick.js')?>"></script>
    <!-- AdminLTE App -->
    <script src="<?=base_url('assets/dist/js/adminlte.min.js')?>"></script>
    <script type="text/javascript">
      $(function () {
        $("#logout").confirm({
          title:"Konfirmasi",
          text:"Keluar dari aplikasi?",
          confirmButton: "Ya",
          cancelButton: "Tidak"
        });
        $("#side_menu").mouseenter(function(){
            $("body").removeClass("sidebar-collapse");
        });
        $("#side_menu").mouseleave(function(){
            $("body").addClass("sidebar-collapse");
        });
      })
    </script>
    <script>
      var notif = '<?=$this->session->flashdata('alert');?>';
      if(notif=='input') {
          Swal.fire({
              icon: 'success',
              text: "Data berhasil disimpan"
          })
      }
      else if(notif=='edit') {
          Swal.fire({
              icon: 'info',
              text: "Data berhasil diperbaharui"
          })
      }
      else if(notif=='delete') {
          Swal.fire({
              icon: 'warning',
              text: "Data berhasil dihapus"
          })
      }
    </script>
  </body>
</html>