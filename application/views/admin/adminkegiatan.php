<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="row">
      <div class="col-md-10 col-sm-8 col-xs-12">
        <h3>
          <i class="fa fa-th-large text-orange"></i> <a href="<?=site_url('admin')?>">SI-Bazzar</a>
          &nbsp;<i class="fa fa-angle-right"></i> Admin Kegiatan
        </h3>
      </div>
      <div class="col-md-2 col-sm-4 col-xs-6 pull-right">
      </div>
    </div>
  </section>

  <!-- Main content -->
  <section class="content">
    
    <div class="row">
        <div class="col-md-12">
            <a href="<?=site_url('admin/tambahadminkegiatan')?>" class="btn btn-success pull-right mb-1"><i class="fa fa-plus"></i> Tambah Admin Kegiatan</a>
        </div>
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Data Kegiatan</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive">
                    <table id="datatable" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th style="width: 2rem;">No.</th>
                            <th>Nama Kegiatan</th>
                            <th>Admin</th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php
                            if(is_array($kegiatan)):
                                $no=1;
                                foreach($kegiatan as $row):
                        ?>
                        <tr>
                            <td><?=$no?>.</td>
                            <td><?=$row->nama_kegiatan?></td>
                            <td>
                                <?php
                                    $ada=0;
                                    if(is_array($admin_kegiatan)) {
                                        $noA=1;
                                        foreach($admin_kegiatan as $rowA) {
                                            if($row->id==$rowA->id_kegiatan) {
                                                echo "$noA. $rowA->username - $rowA->nama <a href='javascript:void(0);' data-params='$rowA->id' class='hapus-data'><i class='fa fa-trash text-red'></i></a></br></br>";
                                                $noA++;
                                                $ada=1;
                                            }
                                        }
                                    }
                                    if($ada==0) {
                                        echo "-";
                                    }
                                ?>
                            </td>
                        </tr>
                        <?php
                                $no++;
                                endforeach;
                            endif;
                        ?>
                    </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script type="text/javascript">
  $(function () {
    $('.admin').addClass('active');
    $('#datatable').DataTable({
        'paging'      : true,
        'lengthChange': true,
        'searching'   : true,
        'ordering'    : true,
        'info'        : true,
        'autoWidth'   : false
      });
    
    $(document).on('click', '.hapus-data', function() {
        Swal.fire({
            title: 'Konfirmasi',
            text: "Hapus Data?",
            icon: 'warning',
        }).then((result) => {
            if (result.value == true) {
                var id = this.dataset.params;
                $.ajax({   
                    type: "POST",
                    dataType: "html",
                    url: "<?=base_url('admin/hapusadminkegiatan/')?>" + id,   
                    success: function(data){
                        data = JSON.parse(data);
                        if(data == "success") {
                            location.reload();
                        }
                    }
                });
            }
        })
    });
  })
</script>