<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="row">
      <div class="col-md-10 col-sm-8 col-xs-12">
        <h3>
          <i class="fa fa-th-large text-orange"></i> <a href="<?=site_url('admin')?>">SI-Bazzar</a>
          &nbsp;<i class="fa fa-angle-right"></i> Edit Penanggung Jawab Kupon
        </h3>
      </div>
      <div class="col-md-2 col-sm-4 col-xs-6 pull-right">
      </div>
    </div>
  </section>

  <!-- Main content -->
  <section class="content">

    <div class="row">
      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <div class="box box-warning">
          <div class="box-header">
            <h3 class="box-title">Edit Penanggung Jawab Kupon</h3>
          </div>
          <?php
            echo form_open('admin/pj/edit/'.$id_kegiatan.'/'.$detail->id, array('method' => 'POST', 'role' => 'form', 'enctype' => 'multipart/form-data', 'class' => 'form-horizontal', 'id' => 'dataform'));
            echo validation_errors();
          ?>
            <div class="box-body">
                <div class=col-md-12>
                    <div class="form-group">
                        <label class="control-label">Nama Penanggung Jawab Kupon</label>
                        <input name="nama" required="" class="form-control" placeholder="Nama Penanggung Jawab Kupon" type="text" value="<?=$detail->nama?>">
                    </div>
                    <div class="form-group">
                        <label class="control-label">Alamat</label>
                        <input name="alamat" class="form-control" placeholder="Alamat" type="text" value="<?=$detail->alamat?>">
                    </div>
                    <div class="form-group">
                        <label class="control-label">No HP</label>
                        <input name="no_hp" class="form-control" placeholder="No HP" type="text" value="<?=$detail->no_hp?>">
                    </div>
              </div>

            </div>
            <!-- /.box-body -->
            <div class="box-footer">
              <div class="pull-right">
                <a href="<?=site_url('admin/kegiatan/'.$id_kegiatan)?>" class="btn btn-default mr-10"><i class="fa fa-times"></i> Batal</a>
                <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Simpan</button>
              </div>
            </div>
            <!-- /.box-footer -->
          </form>
        </div>
        <!-- /.box -->
      </div>
    </div>
    <!-- /.row -->

  </section>
  <!-- /.content -->
</div>
<!-- Jquery Validate -->
<script src="<?=base_url('assets/js/jquery-validate.js')?>"></script>
<script>
$('.dashboard').addClass('active');
$( document ).ready(function() {

  $('#dataform').validate({ // initialize the plugin
    rules: {
      'nama': {
        required: true
      }
    },
    submitHandler: function (form) {
      Swal.fire({
        title: 'Konfirmasi',
        text: "Lanjutkan proses?",
        icon: 'info',
      }).then((result) => {
        if (result.value == true) {
          form.submit();
        }
      })
    }
  });
});
</script>