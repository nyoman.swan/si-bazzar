<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="row">
      <div class="col-md-10 col-sm-8 col-xs-12">
        <h3>
          <i class="fa fa-th-large text-orange"></i> <a href="<?=site_url('admin')?>">SI-Bazzar</a>
          &nbsp;<i class="fa fa-angle-right"></i> Edit User
        </h3>
      </div>
      <div class="col-md-2 col-sm-4 col-xs-6 pull-right">
      </div>
    </div>
  </section>

  <!-- Main content -->
  <section class="content">

    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="box box-warning">
          <div class="box-header">
            <h3 class="box-title">Edit User</h3>
          </div>
          <?php
            echo form_open('admin/edituser/'.$detail->id, array('method' => 'POST', 'role' => 'form', 'enctype' => 'multipart/form-data', 'class' => 'form-horizontal', 'id' => 'dataform'));
            echo validation_errors();
          ?>
            <div class="box-body">
              <div class="form-group row">
                <label class="col-sm-1 control-label">Username</label>
                <div class="col-sm-10">
                  <input name="username" readonly required="" class="form-control" placeholder="Username" type="text" value="<?=$detail->username?>">
                </div>
              </div>

              <div class="form-group row">
                <label class="col-sm-1 control-label">Nama</label>
                <div class="col-sm-10">
                  <input name="nama" required="" class="form-control" placeholder="Nama" type="text" value="<?=$detail->nama?>">
                </div>
              </div>

              <div class="form-group row">
                <label class="col-sm-1 control-label">Alamat</label>
                <div class="col-sm-10">
                  <input name="alamat" required="" class="form-control" placeholder="Alamat" type="text" value="<?=$detail->alamat?>">
                </div>
              </div>

              <div class="form-group row">
                <label class="col-sm-1 control-label">Password</label>
                <div class="col-sm-10">
                  <input name="password" class="form-control" placeholder="Isi untuk mengubah" type="password">
                </div>
              </div>

            </div>
            <!-- /.box-body -->
            <div class="box-footer">
              <div class="pull-right">
                <a href="<?=site_url('admin/kelolauser')?>" class="btn btn-default mr-10"><i class="fa fa-times"></i> Batal</a>
                <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Simpan</button>
              </div>
            </div>
            <!-- /.box-footer -->
          </form>
        </div>
        <!-- /.box -->
      </div>
    </div>
    <!-- /.row -->

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script src="<?=base_url('assets/js/format-currency.js')?>"></script>
<!-- Jquery Validate -->
<script src="<?=base_url('assets/js/jquery-validate.js')?>"></script>
<script>
$('.user').addClass('active');
$( document ).ready(function() {

  $('#dataform').validate({ // initialize the plugin
    rules: {
      'nama': {
        required: true
      },
      'username': {
        required: true
      },
      'alamat': {
        required: true
      },
    },
    submitHandler: function (form) {
      Swal.fire({
        title: 'Konfirmasi',
        text: "Lanjutkan proses?",
        icon: 'info',
      }).then((result) => {
        if (result.value == true) {
          form.submit();
        }
      })
    }
  });
});
</script>