<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="row">
      <div class="col-md-10 col-sm-8 col-xs-12">
        <h3>
          <i class="fa fa-home"></i> <a href="index.php">Dashboard</a>
          &nbsp;<i class="fa fa-angle-right"></i> Admin
        </h3>
      </div>
      <div class="col-md-2 col-sm-4 col-xs-6 pull-right">
      </div>
    </div>
  </section>

  <!-- Main content -->
  <section class="content">

    <div class="row">
      <div class="col-lg-10 col-md-10 col-sm-9 col-xs-10">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">Data Admin</h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body table-responsive">
            <table id="example1" class="table table-bordered table-hover">
              <thead>
              <tr>
                <th>No</th>
                <th>Username</th>
                <th>Nama</th>
                <th>Role</th>
                <th>Created At</th>
                <th style="width: 93px;">Act</th>
              </tr>
              </thead>

              <tbody>
              <tr>
                <td>1</td>
                <td>username</td>
                <td>Elin</td>
                <td>Super Admin</td>
                <td>CAT</td>
                <td align="center">
                  <div class="btn-group">
                    <a href="<?=site_url('admin/edit')?>" class="btn btn-default"><i class="fa fa-edit"></i></a>
                    <a href="<?=site_url('admin/hapus')?>" class="btn btn-default"><i class="fa fa-trash text-red"></i></a>
                  </div>
                </td>
              </tr>
              </tbody>
            </table>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <div class="col-lg-2 col-md-2 col-sm-3 col-xs-2">
        <a href="<?=site_url('admin/tambah')?>" class="btn btn-success btn-block pull-right overflow-hidden"><i class="fa fa-plus"></i> Input Admin</a>
      </div>
    </div>
    <!-- /.row -->

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->