<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="row">
      <div class="col-md-10 col-sm-8 col-xs-12">
        <h3>
          <i class="fa fa-th-large text-orange"></i> <a href="<?=site_url('admin')?>">SI-Bazzar</a>
          &nbsp;<i class="fa fa-angle-right"></i> Tambah Admin Kegiatan
        </h3>
      </div>
      <div class="col-md-2 col-sm-4 col-xs-6 pull-right">
      </div>
    </div>
  </section>

  <!-- Main content -->
  <section class="content">

    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="box box-warning">
          <div class="box-header">
            <h3 class="box-title">Tambah User</h3>
          </div>
          <?php
            echo form_open('admin/tambahadminkegiatan', array('method' => 'POST', 'role' => 'form', 'enctype' => 'multipart/form-data', 'class' => 'form-horizontal', 'id' => 'dataform'));
            echo validation_errors();
          ?>
            <div class="box-body">
              <div class="form-group row">
                <label class="col-sm-1 control-label">Kegiatan</label>
                <div class="col-sm-10">
                  <select name="id_kegiatan" class="form-control">
                    <option value="">Pilih</option>
                    <?php
                        if(is_array($kegiatan)) {
                            foreach($kegiatan as $row) {
                                echo "<option value='$row->id'>$row->nama_kegiatan</option>";
                            }
                        }
                    ?>
                  </select>
                </div>
              </div>

              <div class="form-group row">
                <label class="col-sm-1 control-label">User</label>
                <div class="col-sm-10">
                  <select name="id_user" class="form-control">
                    <option value="">Pilih</option>
                    <?php
                        if(is_array($user)) {
                            foreach($user as $row) {
                                echo "<option value='$row->id'>$row->nama</option>";
                            }
                        }
                    ?>
                  </select>
                </div>
              </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
              <div class="pull-right">
                <a href="<?=site_url('admin/adminkegiatan')?>" class="btn btn-default mr-10"><i class="fa fa-times"></i> Batal</a>
                <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Simpan</button>
              </div>
            </div>
            <!-- /.box-footer -->
          </form>
        </div>
        <!-- /.box -->
      </div>
    </div>
    <!-- /.row -->

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script src="<?=base_url('assets/js/format-currency.js')?>"></script>
<!-- Jquery Validate -->
<script src="<?=base_url('assets/js/jquery-validate.js')?>"></script>
<script>
$('.user').addClass('active');
$( document ).ready(function() {

  $('#dataform').validate({ // initialize the plugin
    rules: {
      'nama': {
        required: true
      },
      'username': {
        required: true
      },
      'alamat': {
        required: true
      },
      'password': {
        required: true
      },
    },
    submitHandler: function (form) {
      Swal.fire({
        title: 'Konfirmasi',
        text: "Lanjutkan proses?",
        icon: 'info',
      }).then((result) => {
        if (result.value == true) {
          form.submit();
        }
      })
    }
  });
});
</script>