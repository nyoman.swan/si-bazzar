<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="row">
      <div class="col-md-10 col-sm-8 col-xs-12">
        <h4>
          <i class="fa fa-home"></i> <a href="<?=base_url()?>">List Kegiatan</a>
          &nbsp;<i class="fa fa-angle-right"></i> <?php echo $kegiatan->nama_kegiatan?>
        </h4>
      </div>
      <div class="col-md-2 col-sm-4 col-xs-6 pull-right">
      </div>
    </div>
  </section>

  <!-- Main content -->
  <section class="content">

    <div class="row">
      <div class="col-md-12">
        <div class="box box-warning">
          <div class="box-body">
            <div class="col-lg-12">
              <div class="form-group pull-right">
                <!-- <a href="<?=site_url('kegiatan/cetak/'.$id_kegiatan)?>" class="btn btn-success"><i class="fa fa-download"></i> Expor Excel</a> -->
                <a href="<?=site_url('kupon/tambah/'.$id_kegiatan)?>" class="btn btn-info"><i class="fa fa-plus"></i> Tambah Tipe Kupon</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-12">
        <div class="box box-warning">
          <div class="box-header">
            <h3 class="box-title">Tipe Kupon</h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <table class="table table-bordered table-hover datatable">
              <thead>
              <tr>
                <th>No</th>
                <th>Tipe Kupon</th>
                <th>Jumlah</th>
                <th>Harga Satuan</th>
                <th style="width: 100px;">Aksi</th>
              </tr>
              </thead>
              <tbody>

                <?php
                  $no=1;
                  if(is_array($tipe_kupon) || is_object($tipe_kupon)) :
                  foreach ($tipe_kupon as $row) :
                ?>

                <tr>
                  <td><?=$no?>.</td>
                  <td><?=$row->keterangan?></td>
                  <td><?=number_format($row->jumlah_kupon, 0, ',', '.') . ' lembar'?></td>
                  <td><?='Rp. ' . number_format($row->harga, 0, ',', '.')?></td>
                  <td align="center">
                    <div class="btn-group">
                      <a href="<?=site_url('kupon/edit/'.$row->id_tipe)?>" class="btn btn-default"><i class="fa fa-edit"></i></a>
                      <a href="<?=site_url('kupon/hapus/'.$row->id_tipe)?>" class="btn btn-default delete"><i class="fa fa-trash text-red"></i></a>
                    </div>
                  </td>
                </tr>

              <?php
                $no++;
                endforeach;
                endif;
              ?>

              </tbody>
            </table>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
    </div>

    <div class="row">
      <div class="col-md-12">
        <div class="box box-warning">
          <div class="box-body">
            <div class="col-lg-12">
              <div class="form-group pull-right">
                <a href="<?=site_url('kupon/multipleedit/'.$id_kegiatan)?>" class="btn btn-warning"><i class="fa fa-bolt"></i> Multiple Edit Kupon</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-12">
        <div class="box box-warning">
          <div class="box-header">
            <h3 class="box-title">Data Kupon</h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <table class="table table-bordered table-hover datatable">
              <thead>
              <tr>
                <th>No Seri</th>
                <th>Tipe Kupon</th>
                <th>Penanggung Jawab</th>
                <th>Tujuan</th>
                <th>Harga</th>
                <th>Status</th>
                <th style="width: 100px;">Aksi</th>
              </tr>
              </thead>
              <tbody>

                <?php
                  if(is_array($kupon) || is_object($kupon)) :
                  foreach ($kupon as $row) :
                ?>

                <tr>
                  <td><?=$row->no_seri_kupon?></td>
                  <td><?=$row->keterangan?></td>
                  <td>
                    <?php
                      if($row->nama==null)
                      {
                        echo "-";
                      } 
                      else
                      {
                        echo $row->nama . " - " . $row->alamat . "<br/>(" . $row->no_hp . ")";
                      }
                    ?>  
                  </td>
                  <td>
                    <?php
                      if($row->tujuan==null)
                      {
                        echo "-";
                      } 
                      else
                      {
                        echo $row->tujuan;
                      }
                    ?>  
                  </td>
                  <td><?='Rp. ' . number_format($row->harga, 0, ',', '.')?></td>
                  <td>
                    <?php
                      if($row->status_kupon == 1)
                      {
                        echo "<i class='fa fa-circle-o text-gray mr-5'></i> Belum disebar";
                      }
                      else if($row->status_kupon == 2)
                      {
                        echo "<i class='fa fa-clock-o text-aqua mr-5'></i> Proses";
                      }
                      else if($row->status_kupon == 3)
                      {
                        echo "<i class='fa fa-circle-o-notch fa-spin text-orange mr-5'></i> Kembali";
                      }
                      else if($row->status_kupon == 4)
                      {
                        echo "<i class='fa fa-check text-green mr-5'></i> Lunas";
                      }
                    ?>
                  </td>
                  <td align="center">
                    <div class="btn-group">
                      <a href="<?=site_url('kupon/kembali/'.$row->id_kupon.'_'.$id_kegiatan)?>" class="btn btn-default kembali"><i class="fa fa-spin fa-circle-o-notch text-orange"></i></a>
                      <a href="<?=site_url('kupon/lunas/'.$row->id_kupon.'_'.$id_kegiatan)?>" class="btn btn-default lunas"><i class="fa fa-check text-green"></i></a>
                    </div>
                  </td>
                </tr>

              <?php
                $no++;
                endforeach;
                endif;
              ?>

              </tbody>
            </table>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
    </div>

  </section>
  <!-- /.content -->
</div>

<!-- /.content-wrapper -->
<script type="text/javascript">
$('.dashboard').addClass('active');
  $(function () {
    $('.datatable').DataTable({
        'stateSave'   : true,
        'pageLength'  : 25,
        'paging'      : true,
        'lengthChange': true,
        'searching'   : true,
        'ordering'    : true,
        'info'        : true,
        'autoWidth'   : false
      })
    $(".kembali").confirm({
      title:"Konfirmasi",
      text:"Set jadi kupon kembali?",
      confirmButton: "<i class='fa fa-check'></i> Ya",
      cancelButton: "<i class='fa fa-times'></i> Tidak"
    });
    $(".lunas").confirm({
      title:"Konfirmasi",
      text:"Set jadi kupon lunas?",
      confirmButton: "<i class='fa fa-check'></i> Ya",
      cancelButton: "<i class='fa fa-times'></i> Tidak"
    });
  });
</script>