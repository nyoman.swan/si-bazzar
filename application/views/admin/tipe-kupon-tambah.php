<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="row">
      <div class="col-md-10 col-sm-8 col-xs-12">
        <h4>
          <i class="fa fa-home"></i> <a href="<?=base_url()?>">List Kegiatan</a>
          &nbsp;<i class="fa fa-angle-right"></i> <a href="<?=base_url('kegiatan/detail/'.$kegiatan->id_kegiatan)?>"><?=$kegiatan->nama_kegiatan?></a>
          &nbsp;<i class="fa fa-angle-right"></i> Tambah Tipe Kupon
        </h4>
      </div>
      <div class="col-md-2 col-sm-4 col-xs-6 pull-right">
      </div>
    </div>
  </section>

  <!-- Main content -->
  <section class="content">

    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="box box-warning">
          <div class="box-header">
            <h3 class="box-title">Tambah Tipe Kupon</h3>
          </div>
          <?php
            echo form_open('kupon/tambah/'.$id_kegiatan, array('method' => 'POST', 'role' => 'form', 'enctype' => 'multipart/form-data', 'class' => 'form-horizontal'));
            echo validation_errors();
          ?>
            <div class="box-body">
              <div class="form-group">
                <label class="col-sm-2 control-label">Keterangan</label>
                <div class="col-sm-9">
                  <input type="hidden" name="id_kegiatan" value="">
                  <input required="" name="keterangan" required="" class="form-control" placeholder="Keterangan" value="" type="text">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label">Harga Satuan</label>
                <div class="col-sm-4">
                  <input required="" name="harga" required="" class="form-control" placeholder="Harga Satuan" value="" type="text" data-type="currency">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label">Jumlah Kupon</label>
                <div class="col-sm-4">
                  <input required="" name="jumlah" required="" class="form-control" placeholder="Jumlah Kupon" value="" type="text" data-type="currency">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label">Jumlah Digit No Seri</label>
                <div class="col-sm-4">
                  <input required="" name="digit" required="" class="form-control" placeholder="Jumlah Digit No Seri. Misal: 5" value="" type="number">
                </div>
              </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
              <div class="pull-right">
                <a href="<?=site_url('kegiatan/detail/'.$id_kegiatan)?>" class="btn btn-default mr-10"><i class="fa fa-times"></i> Batal</a>
                <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Simpan</button>
              </div>
            </div>
            <!-- /.box-footer -->
          </form>
        </div>
        <!-- /.box -->
      </div>
    </div>
    <!-- /.row -->

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<script type="text/javascript">
$('.dashboard').addClass('active');
  // Jquery Dependency

$("input[data-type='currency']").on({
    keyup: function() {
      formatCurrency($(this));
    },
    blur: function() { 
      formatCurrency($(this), "blur");
    }
});


function formatNumber(n) {
  // format number 1000000 to 1,234,567
  return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",")
}

function formatCurrency(input, blur) {
  // appends $ to value, validates decimal side
  // and puts cursor back in right position.
  
  // get input value
  var input_val = input.val();
  
  // don't validate empty input
  if (input_val === "") { return; }
  
  // original length
  var original_len = input_val.length;

  // initial caret position 
  var caret_pos = input.prop("selectionStart");
    
  // check for decimal
  if (input_val.indexOf(".") >= 0) {

    // get position of first decimal
    // this prevents multiple decimals from
    // being entered
    var decimal_pos = input_val.indexOf(".");

    // split number by decimal point
    var left_side = input_val.substring(0, decimal_pos);
    var right_side = input_val.substring(decimal_pos);

    // add commas to left side of number
    left_side = formatNumber(left_side);

    // validate right side
    right_side = formatNumber(right_side);
    
    // On blur make sure 2 numbers after decimal
    if (blur === "blur") {
      right_side;
    }
    
    // Limit decimal to only 2 digits
    right_side = right_side.substring(0, 2);

    // join number by .
    input_val = left_side + "." + right_side;

  } else {
    // no decimal entered
    // add commas to number
    // remove all non-digits
    input_val = formatNumber(input_val);
    input_val = input_val;
    
    // final formatting
    if (blur === "blur") {
      input_val;
    }
  }
  
  // send updated string to input
  input.val(input_val);

  // put caret back in the right position
  var updated_len = input_val.length;
  caret_pos = updated_len - original_len + caret_pos;
  input[0].setSelectionRange(caret_pos, caret_pos);
}
</script>