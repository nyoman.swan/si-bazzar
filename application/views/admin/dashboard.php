<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="row">
      <div class="col-md-10 col-sm-8 col-xs-12">
        <h3>
          <i class="fa fa-th-large text-orange"></i> <a href="<?=site_url('admin')?>">SI-Bazzar</a>
          &nbsp;<i class="fa fa-angle-right"></i> Dashboard
        </h3>
      </div>
      <div class="col-md-2 col-sm-4 col-xs-6 pull-right">
      </div>
    </div>
  </section>

  <!-- Main content -->
  <section class="content">

    <div class="row">
        <div class="col-md-12">
            <div class="box box-success box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Halo <?=$this->session->userdata($this->appsession->get())['nama'];?> !</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    Selamat datang di aplikasi SI-Bazzar (つ▀¯▀)つ
                </div>
            </div>
        </div>
    </div>
    
    <div class="row">
        <?php if($this->session->userdata($this->appsession->get())['is_admin']): ?>
        <div class="col-md-12">
            <a href="<?=site_url('admin/kegiatan/tambah')?>" class="btn btn-success pull-right mb-1"><i class="fa fa-plus"></i> Tambah Kegiatan</a>
        </div>
        <?php endif; ?>
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Data Kegiatan</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive">
                    <table id="datatable" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th style="width: 2rem;">No.</th>
                            <th>Nama Kegiatan</th>
                            <th>Jenis Kupon</th>
                            <th>Admin</th>
                            <th>Created At</th>
                            <th style="width: 14rem;">Act</th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php
                            if(is_array($kegiatan)):
                                $no=1;
                                foreach($kegiatan as $row):
                        ?>
                        <tr>
                            <td><?=$no?>.</td>
                            <td><a href="<?=site_url('admin/kegiatan/'.$row->id)?>"><?=$row->nama_kegiatan?> <i class="fa fa-arrow-circle-right text-green"></i></a></td>
                            <td>
                                <?php
                                    $ada=0;
                                    if(is_array($jenis_kupon)) {
                                        $noA=1;
                                        foreach($jenis_kupon as $rowA) {
                                            if($row->id==$rowA->id_kegiatan) {
                                                echo "$noA. $rowA->keterangan @$rowA->jumlah Lembar </br>";
                                                $noA++;
                                                $ada=1;
                                            }
                                        }
                                    }
                                    if($ada==0) {
                                        echo "-";
                                    }
                                ?>
                            </td>
                            <td>
                                <?php
                                    $ada=0;
                                    if(is_array($admin_kegiatan)) {
                                        $noA=1;
                                        foreach($admin_kegiatan as $rowA) {
                                            if($row->id==$rowA->id_kegiatan) {
                                                echo "$noA. $rowA->username - $rowA->nama </br>";
                                                $noA++;
                                                $ada=1;
                                            }
                                        }
                                    }
                                    if($ada==0) {
                                        echo "-";
                                    }
                                ?>
                            </td>
                            <td><?=$row->created_at_f?></td>
                            <td align="center">
                            <div class="btn-group">
                                <a href="<?=site_url('admin/kegiatan/'.$row->id)?>" class="btn btn-default"><i class="fa fa-sign-in text-blue"></i></a>
                                <a href="<?=site_url('admin/kegiatan/edit/'.$row->id)?>" class="btn btn-default"><i class="fa fa-edit"></i></a>
                                <?php if($this->session->userdata($this->appsession->get())['is_admin']): ?>
                                <a href="javascript:void(0);" data-params="<?=$row->id?>" class="btn btn-default hapus-data"><i class="fa fa-trash text-red"></i></a>
                                <?php endif; ?>
                            </div>
                            </td>
                        </tr>
                        <?php
                                $no++;
                                endforeach;
                            endif;
                        ?>
                    </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script type="text/javascript">
  $(function () {
    $('.dashboard').addClass('active');
    $('#datatable').DataTable({
        'paging'      : true,
        'lengthChange': true,
        'searching'   : true,
        'ordering'    : true,
        'info'        : true,
        'autoWidth'   : false
      });
    
    $(document).on('click', '.hapus-data', function() {
        Swal.fire({
            title: 'Konfirmasi',
            text: "Hapus Data?",
            icon: 'warning',
        }).then((result) => {
            if (result.value == true) {
                var id = this.dataset.params;
                $.ajax({   
                    type: "POST",
                    dataType: "html",
                    url: "<?=base_url('admin/hapuskegiatan/')?>" + id,   
                    success: function(data){
                        data = JSON.parse(data);
                        if(data == "success") {
                            location.reload();
                        }
                    }
                });
            }
        })
    });
  })
</script>