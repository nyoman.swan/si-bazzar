<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="row">
      <div class="col-md-10 col-sm-8 col-xs-12">
        <h3>
          <i class="fa fa-home"></i> <a href="index.php">Dashboard</a>
          &nbsp;<i class="fa fa-angle-right"></i> Peserta
        </h3>
      </div>
      <div class="col-md-2 col-sm-4 col-xs-6 pull-right">
      </div>
    </div>
  </section>

  <!-- Main content -->
  <section class="content">

    <div class="row">
      <div class="col-lg-10 col-md-10 col-sm-9 col-xs-10">
        <div class="box box-warning">
          <div class="box-header">
            <h3 class="box-title">Tambah Peserta</h3>
          </div>
          <form class="form-horizontal">
              <div class="box-body">
                <div class="form-group">
                  <label class="col-sm-2 control-label">Role Admin</label>
                  <div class="col-sm-4">
                    <select name="role" class="form-control">
                      <option value="0">Semua Role</option>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">Username Admin</label>
                  <div class="col-sm-9">
                    <input name="username" class="form-control" placeholder="Username Admin" type="text">
                  </div>
                </div>             
                <div class="form-group">
                  <label class="col-sm-2 control-label">Nama Admin</label>
                  <div class="col-sm-9">
                    <input name="nama" class="form-control" placeholder="Nama Admin" type="text">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">Password</label>
                  <div class="col-sm-9">
                    <input name="password" class="form-control" placeholder="Password" type="password">
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <div class="pull-right">
                  <a href="<?=site_url('admin')?>" class="btn btn-default mr-10"><i class="fa fa-times"></i> Batal</a>
                  <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Simpan</button>
                </div>
              </div>
              <!-- /.box-footer -->
            </form>
        </div>
        <!-- /.box -->
      </div>
      <div class="col-lg-2 col-md-2 col-sm-3 col-xs-2">
        <a href="<?=site_url('admin')?>" class="btn btn-warning btn-block pull-right overflow-hidden"><i class="fa fa-reply mr-5"></i> Kembali</a>
      </div>
    </div>
    <!-- /.row -->

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->