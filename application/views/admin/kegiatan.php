<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="row">
      <div class="col-md-10 col-sm-8 col-xs-12">
        <h3>
          <i class="fa fa-th-large text-orange"></i> <a href="<?=site_url('admin')?>">SI-Bazzar</a>
          &nbsp;<i class="fa fa-angle-right"></i> <?=$detail->nama_kegiatan?>
        </h3>
      </div>
      <div class="col-md-2 col-sm-4 col-xs-6 pull-right">
      </div>
    </div>
  </section>

  <!-- Main content -->
  <section class="content">

  
    <?php
      $belum_beredar = 0;
      $jumlah_belum_beredar = 0;
      $total_disebar = 0;
      $jumlah_disebar = 0;
      $total_kembali = 0;
      $jumlah_kembali = 0;
      $total_lunas = 0;
      $jumlah_lunas = 0;
      if(is_array($jenis_kupon)):
        foreach($jenis_kupon as $row):
          $belum_beredar =  $belum_beredar + ($row->jumlah_belum_beredar*$row->harga);
          $jumlah_belum_beredar = $jumlah_belum_beredar + $row->jumlah_belum_beredar;
          $total_disebar = $total_disebar + ($row->j_disebar*$row->harga);
          $jumlah_disebar = $jumlah_disebar + $row->j_disebar;
          $total_kembali = $total_kembali + ($row->j_kembali*($row->harga-$row->harga_pokok));
          $jumlah_kembali = $jumlah_kembali + $row->j_kembali;
          $total_lunas = $total_lunas + ($row->j_lunas*$row->harga);
          $jumlah_lunas = $jumlah_lunas + $row->j_lunas;
        endforeach;
      endif;
    ?>

    <div class="row">
      <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box bg-yellow">
              <span class="info-box-icon"><i class="ion ion-ios-pricetag-outline"></i></span>
              <div class="info-box-content">
                  <span class="info-box-text">Kupon Belum Beredar</span>
                  <span class="info-box-number">Rp. <?=number_format($belum_beredar, 0, ',', '.')?></span>
                  <div class="progress">
                      <div class="progress-bar" style="width: 100%"></div>
                  </div>
                  <span class="progress-description"><?=$jumlah_belum_beredar?> Lembar</span>
              </div>
          </div>
      </div>

      <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box bg-aqua">
              <span class="info-box-icon"><i class="ion ion-ios-pricetag-outline"></i></span>
              <div class="info-box-content">
                  <span class="info-box-text">Kupon Beredar</span>
                  <span class="info-box-number">Rp. <?=number_format($total_disebar, 0, ',', '.')?></span>
                  <div class="progress">
                      <div class="progress-bar" style="width: 100%"></div>
                  </div>
                  <span class="progress-description"><?=$jumlah_disebar?> Lembar</span>
              </div>
          </div>
      </div>

      <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box bg-red">
              <span class="info-box-icon"><i class="ion ion-ios-pricetag-outline"></i></span>
              <div class="info-box-content">
                  <span class="info-box-text">Kupon Kembali</span>
                  <span class="info-box-number">Rp. <?=number_format($total_kembali, 0, ',', '.')?></span>
                  <div class="progress">
                      <div class="progress-bar" style="width: 100%"></div>
                  </div>
                  <span class="progress-description"><?=$jumlah_kembali?> Lembar</span>
              </div>
          </div>
      </div>

      <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box bg-green">
              <span class="info-box-icon"><i class="ion ion-ios-pricetag-outline"></i></span>
              <div class="info-box-content">
                  <span class="info-box-text">Kupon Lunas</span>
                  <span class="info-box-number">Rp. <?=number_format($total_lunas, 0, ',', '.')?></span>
                  <div class="progress">
                      <div class="progress-bar" style="width: 100%"></div>
                  </div>
                  <span class="progress-description"><?=$jumlah_lunas?> Lembar</span>
              </div>
          </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-12">
        <div class="pull-right">
          <a href="<?=site_url('admin/kegiatan/edit/'.$detail->id)?>" class="btn btn-warning mb-2 mr-1"><i class="fa fa-pencil"></i> Edit Kegiatan</a>
          <a href="<?=site_url('admin/pj/tambah/'.$detail->id)?>" class="btn btn-primary mb-2 mr-1"><i class="fa fa-plus"></i> Tambah Penanggung Jawab</a>
          <?php if($this->session->userdata($this->appsession->get())['is_admin']): ?>
          <a href="#" data-params="<?=$detail->id?>" class="btn btn-danger mb-2 hapus-data"><i class="fa fa-trash"></i> Hapus Kegiatan</a>
          <?php endif; ?>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-12">
        <div class="box box-warning">
          <div class="box-header with-border">
            <h3 class="box-title">Rekap Per Jenis Kupon</h3>
            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
              </button>
            </div>
            <!-- /.box-tools -->
          </div>
          <!-- /.box-header -->
          <div class="box-body table-responsive" style="">
            <table class="table table-bordered">
              <tbody>
                <tr>
                  <th style="width: 10px">#</th>
                  <th>Jenis</th>
                  <th>Harga</th>
                  <th>Jumlah</th>
                  <th>Beredar</th>
                  <th>Kembali</th>
                  <th>Rp. Kupon</th>
                  <th>Lunas</th>
                  <th>Rp. Lunas</th>
                </tr>
                <?php 
                  if(is_array($jenis_kupon)):
                    $no=1;
                    foreach($jenis_kupon as $row):
                      $p_disebar = number_format($row->j_disebar/$row->jumlah, 2) . "%";
                      $p_kembali = number_format($row->j_kembali/$row->jumlah, 2) . "%";
                      $p_lunas = number_format($row->j_lunas/$row->jumlah, 2) . "%";
                ?>
                <tr>
                  <td><?=$no?>.</td>
                  <td><?=$row->keterangan?></td>
                  <td>Rp. <?=number_format($row->harga, 0, ',', '.')?></td>
                  <td><span class="ml-1 badge bg-grey"><?=$row->jumlah?> Lembar</span></td>
                  <td><?=$row->j_disebar?> Lembar <small><span class="ml-1 badge bg-aqua"><?=$p_disebar?></span></small></td>
                  <td><?=$row->j_kembali?> Lembar <small><span class="ml-1 badge bg-red"><?=$p_kembali?></span></small></td>
                  <td><span class="ml-1 badge bg-red">Rp. <?=number_format($row->j_kembali*($row->harga-$row->harga_pokok), 0, ',', '.')?></span></td>
                  <td><?=$row->j_lunas?> Lembar <small><span class="ml-1 badge bg-green"><?=$p_lunas?></span></small></td>
                  <td><span class="ml-1 badge bg-green">Rp. <?=number_format($row->j_lunas*$row->harga, 0, ',', '.')?></span></td>
                </tr>
                <?php
                      $no++;
                    endforeach;
                  endif;
                ?>
              </tbody>
            </table>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
    </div>

    <div class="row">
      <div class="col-md-12">
        <div class="box box-warning collapsed-box">
          <div class="box-header with-border">
            <h3 class="box-title">Rekap Per Penanggung Jawab Kupon</h3>
            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
              </button>
            </div>
            <!-- /.box-tools -->
          </div>
          <!-- /.box-header -->
          <div class="box-body table-responsive" style="">            
            <table class="table data-table table-bordered">
              <thead>
                <tr>
                  <th style="width: 1rem">No</th>
                  <th>Nama</th>
                  <th>Alamat</th>
                  <th>No HP</th>
                  <th>Jumlah</th>
                  <th>Kembali</th>
                  <th>Lunas</th>
                  <th style="width: 8rem;">Act</th>
                </tr>
              </thead>
              <tbody>
                <?php 
                  if(is_array($by_pj)):
                    $no=1;
                    foreach($by_pj as $row):
                ?>
                <tr>
                  <td><?=$no?>.</td>
                  <td><?=$row->nama?></td>
                  <td><?=$row->alamat?></td>
                  <td><?=$row->no_hp?></td>
                  <td><span class="ml-1 badge bg-grey"><?=$row->jumlah?> Lembar<span></td>
                  <td><span class="ml-1 badge bg-red"><?=$row->j_kembali?> Lembar<span></td>
                  <td><span class="ml-1 badge bg-green"><?=$row->j_lunas?> Lembar<span></td>
                  <td align="center">
                    <div class="btn-group">
                        <a href="<?=site_url('admin/pj/edit/'.$detail->id.'/'.$row->id)?>" class="btn btn-default"><i class="fa fa-edit"></i></a>
                        <a href="javascript:void(0);" data-params="<?=$row->id?>" class="btn btn-default hapus-pj"><i class="fa fa-trash text-red"></i></a>
                    </div>
                  </td>
                </tr>
                <?php
                      $no++;
                    endforeach;
                  endif;
                ?>
              </tbody>
            </table>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
    </div>

    <div class="row">
      <div class="col-md-12">
        <div class="pull-right">
          <a href="#" id="reset-checkbox" class="btn btn-default mb-2"><i class="mr-05 fa fa-refresh text-orange"></i> Reset Pilihan</a>
          <a href="#" data-toggle="modal" data-target="#modal-pj" class="btn btn-default mb-2"><i class="mr-05 fa fa-user text-blue"></i> Edit PJ Kupon</a>
          <a href="#" data-toggle="modal" data-target="#modal-status" class="btn btn-success mb-2"><i class="mr-05 fa fa-pencil"></i> Edit Status Kupon</a>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-12">
        <div class="box box-warning">
          <div class="box-header">
            <h3 class="box-title">Data Kupon</h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body table-responsive">
            <table id="table-kupon" class="table table-bordered table-hover">
              <thead>
              <tr>
                <th style="width: 2rem;"><input class="check-all" type="checkbox"></th>
                <th>No Seri Kupon</th>
                <th>Tipe</th>
                <th>Harga</th>
                <th>Nama PJ</th>
                <th>Alamat PJ</th>
                <th>No HP PJ</th>
                <th>Edited By</th>
                <th>Status</th>
              </tr>
              </thead>
              <!-- <tbody>

                <?php
                  $no=1;
                  if(is_array($kupon) || is_object($kupon)) :
                  foreach ($kupon as $row) :
                ?>

                <tr>
                  <td><?=$no?>.</td>
                  <td><a href="<?=site_url('kegiatan/detail/'.$row->id_kegiatan)?>"><?=$row->nama_kegiatan?></a></td>
                  <td align="center">
                    <div class="btn-group">
                      <a href="<?=site_url('kegiatan/edit/'.$row->id_kegiatan)?>" class="btn btn-default"><i class="fa fa-edit"></i></a>
                      <a href="#" data-params="<?=$row->id?>" class="btn btn-default hapus-data"><i class="fa fa-trash text-red"></i></a>
                    </div>
                  </td>
                </tr>

              <?php
                $no++;
                endforeach;
                endif;
              ?>

              </tbody> -->
            </table>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
    </div>
    <!-- /.row -->
    
    <div class="modal fade pt-10-per" id="modal-pj" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title dp-inline"><i class="fa fa-user text-blue"></i> Edit Penanggung Jawab Kupon</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <?php
            echo form_open('admin/edit/pj/'.$detail->id, array('method' => 'POST', 'role' => 'form', 'enctype' => 'multipart/form-data', 'class' => 'form-horizontal', 'id' => 'formpj'));
          ?>
          <div class="modal-body">
            <div class="col-md-12">
              <div class="form-group">
                <label>Penanggung Jawab Kupon</label>
                <select name="pj" class="form-control select2" style="width: 100%;">
                  <option disabled selected value="">Pilih PJ</option>
                  <?php
                    if(is_array($pj)):
                      foreach($pj as $row):
                        echo "<option value='$row->id'>$row->nama | $row->alamat | -</option>";
                      endforeach;
                    endif;
                  ?>
                </select>
              </div>
              
              <div class="form-group">
                <label>Kupon Yang Dipilih</label>
                <input type="hidden" class="kupon-dipilih" name="kupon_dipilih" value="">
                <div class="container-kupon"><p>Tidak ada kupon dipilih</p></div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times"></i> Batal</button>
            <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Simpan</button>
          </div>
          </form>
        </div>
      </div>
    </div>

    <div class="modal fade pt-10-per" id="modal-status" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title dp-inline"><i class="fa fa-pencil text-green"></i> Edit Status Kupon</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <?php
            echo form_open('admin/edit/status/'.$detail->id, array('method' => 'POST', 'role' => 'form', 'enctype' => 'multipart/form-data', 'class' => 'form-horizontal', 'id' => 'formstatus'));
          ?>
          <div class="modal-body">
            <div class="col-md-12">
              <div class="form-group">
                <label>Status Kupon</label>
                <div class="radio">
                  <label for="status1">
                    <input type="radio" name="status_kupon" id="status1" value="1"> Belum Beredar
                  </label>
                </div>
                <div class="radio">
                  <label for="status2">
                    <input type="radio" name="status_kupon" id="status2" value="2"> Beredar <i class="ml-1 fa fa-paper-plane text-blue"></i>
                  </label>
                </div>
                <div class="radio">
                  <label for="status3">
                    <input type="radio" name="status_kupon" id="status3" value="3"> Lunas <i class="ml-1 fa fa-check text-green"></i>
                  </label>
                </div>
                <div class="radio">
                  <label for="status4">
                    <input type="radio" name="status_kupon" id="status4" value="4"> Kembali <i class="ml-1 fa fa-clock-o text-red"></i>
                  </label>
                </div>
              </div>

              <div class="form-group">
                <label>Kupon Yang Dipilih</label>
                <input type="hidden" class="kupon-dipilih" name="kupon_dipilih" value="">
                <div class="container-kupon"><p>Tidak ada kupon dipilih</p></div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times"></i> Batal</button>
            <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Simpan</button>
          </div>
          </form>
        </div>
      </div>
    </div>

  </section>
  <!-- /.content -->
</div>

<!-- Jquery Validate -->
<script src="<?=base_url('assets/js/jquery-validate.js')?>"></script>

<script>
$('.dashboard').addClass('active');
$( document ).ready(function() {
  $('.select2').select2()
  $(".data-table").DataTable();

  var id_kegiatan = "<?=$detail->id?>";
  var table = $("#table-kupon").DataTable({
    autoWidth: false,
    processing: true,
    serverSide: true,
    stateSave: true,
    ajax:{
      url :"<?=site_url('admin/ajaxkupon/')?>" + id_kegiatan, // json datasource
      type: "post",  // method  , by default get
      error: function(){  // error handling
        $(".data-table-error").html("");
        $(".data-table").append('<tbody class="data-table-error text-center"><tr><th colspan="7">No data found in the server</th></tr></tbody>');
        $("#employee-grid_processing").css("display","none");
      }
    },
    columnDefs: [
      {
        'targets': 0,
        'searchable': false,
        'orderable': false,
        'className': 'dt-body-center',
        'render': function (data, type, full, meta){
          return '<input class="check-kupon" type="checkbox" id="checkbox-' + data + '" name="id_kupon[]" value="' + $('<div/>').text(data).html() + '">';
        },
      },
      {
        'targets': 1,
        'className': 'text-center',
        'render': function (data, type, full, meta){
          return '<label for="checkbox-' + data + '">' + data + '</label>';
        },
      }
    ],
    paging: true
  });

  // Handle click on checkbox to set state of "Select all" control
  $(document).on('change', 'input[type="checkbox"]', function(e){
    e.preventDefault();
    // clear container
    $(".container-kupon").html("");
    // get all checked checkbox ID
    var searchIDs = $("#table-kupon input.check-kupon:checkbox:checked").map(function(){
      return $(this).val();
    }).get();
    // jsonify var
    var jsonID = JSON.stringify(searchIDs);
    // pupulate to selected and value
    searchIDs.forEach(function(item) {
      var html = '<span class="mr-02 mb-04 badge bg-grey">' + item + '</span>';
      $(".container-kupon").append(html);
      $(".kupon-dipilih").val(jsonID);
    });
  });

  // Reset checkbox
  $(document).on('click', '#reset-checkbox', function(e){
    e.preventDefault();
    $("#table-kupon input:checkbox").prop('checked', false);
    $(".container-kupon").html("");
    $(".container-kupon").append("<p>Tidak ada kupon dipilih</p>")
    $(".kupon-dipilih").val("");
  });

  // check all
  $(document).on('click', '.check-all', function(event) {
    if($('.check-all:checked').length){
      $(".check-kupon").prop('checked', true);
    }
    else {
      $(".check-kupon").prop('checked', false);
    }
  });

  $('#formpj').validate({ // initialize the plugin
    rules: {
      'pj': {
        required: true
      },
      'kupon_dipilih': {
        required: true
      }
    },
    submitHandler: function (form) {
      Swal.fire({
        title: 'Konfirmasi',
        text: "Lanjutkan proses?",
        icon: 'info',
      }).then((result) => {
        if (result.value == true) {
          form.submit();
        }
      })
    }
  });

  $('#formstatus').validate({ // initialize the plugin
    rules: {
      'status_kupon': {
        required: true
      },
      'kupon_dipilih': {
        required: true
      }
    },
    submitHandler: function (form) {
      Swal.fire({
        title: 'Konfirmasi',
        text: "Lanjutkan proses?",
        icon: 'info',
      }).then((result) => {
        if (result.value == true) {
          form.submit();
        }
      })
    }
  });

  $(document).on('click', '.hapus-data', function(e) {
    e.preventDefault();
    Swal.fire({
      title: 'Konfirmasi',
      text: "Hapus Data?",
      icon: 'warning',
    }).then((result) => {
      if (result.value == true) {
        var id = this.dataset.params;
        $.ajax({   
          type: "POST",
          dataType: "html",
          url: "<?=base_url('admin/hapuskegiatan/')?>" + id,   
          success: function(data){
            data = JSON.parse(data);
            if(data == "success") {
              window.location.href = "<?=site_url('admin')?>";
            }
          }
        });
      }
    })
  });

  $(document).on('click', '.hapus-pj', function(e) {
    e.preventDefault();
    Swal.fire({
      title: 'Konfirmasi',
      text: "Hapus Data?",
      icon: 'warning',
    }).then((result) => {
      if (result.value == true) {
        var id = this.dataset.params;
        $.ajax({   
          type: "POST",
          dataType: "html",
          url: "<?=base_url('admin/hapuspj/')?>" + id,   
          success: function(data){
            data = JSON.parse(data);
            if(data == "success") {
              location.reload();
            }
          }
        });
      }
    })
  });
});
</script>