<!DOCTYPE html>
<html style="height: 0%;">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>SI Bazzar</title>
  <link rel="shortcut icon" href="<?=base_url('assets/dist/img/favicon.png')?>">
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?=base_url('assets/bower_components/bootstrap/dist/css/bootstrap.min.css')?>">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?=base_url('assets/bower_components/font-awesome/css/font-awesome.min.css')?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?=base_url('assets/dist/css/AdminLTE.min.css')?>">

  <!-- Google Font -->
  <link rel="stylesheet" href="<?=base_url('assets/dist/fonts-googleapis.com')?>">
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="<?=site_url('dashboard')?>"><b>SI</b> BAZZAR</a>
    <p style="font-size: 14px;">Aplikasi Monitoring Kupon Bazzar</p>
  </div>
  <!-- /.login-logo -->
  <div style="padding-left: 40px; padding-right: 40px; border-radius: 8px;" class="login-box-body">
    <p class="login-box-msg">Login untuk masuk ke aplikasi</p>

    <?php echo form_open('login', array('method' => 'POST', 'role' => 'form'));?>
      <?php
        // if(validation_errors()){
          echo "<div style='margin-bottom: 15px;' class='text-center'>";
          echo validation_errors("<span class='label bg-red'><i class='icon-cross3'></i>", "</span>");
          echo "</div>";
        // }
      ?>
      <div class="form-group has-feedback">
        <input name="username" type="username" class="form-control" placeholder="Username">
        <span class="fa fa-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input name="pass" type="password" class="form-control" placeholder="Password">
        <span class="fa fa-lock form-control-feedback text-green"></span>
      </div>
      <div class="row">
        <div class="col-xs-3"></div>
        <!-- /.col -->
        <div class="col-xs-6">
          <button type="submit" class="btn btn-warning btn-block btn-flat"><i class="fa fa-check"></i> Sign In</button>
        </div>
        <!-- /.col -->
        <div class="col-xs-3"></div>
      </div>
    </form>
  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 3 -->
<script src="<?=base_url('assets/bower_components/jquery/dist/jquery.min.js')?>"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?=base_url('assets/bower_components/bootstrap/dist/js/bootstrap.min.js')?>"></script>
</body>
<footer class="text-center">
    <strong>&copy; 2017 - <?=date('Y')?> | Purwarupa oleh: <a href="https://www.instagram.com/nyoman_elin/" target="_blank">Nyoman Swantara</a> | <a href="https://www.instagram.com/st.wira_karya_kapal/" target="_blank">Sekaa Teruna Wira Karya Br Cepaka Kapal</a> | <a href="mailto:nyoman.swan@gmail.com"><i class="fa fa-envelope-o"></i> nyoman.swan@gmail.com</a></strong> <b>Versi</b> 1.0.0 c
</footer>
</html>
